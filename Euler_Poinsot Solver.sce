// Euler-Poinsot Solver
// Developer: Cássio Murakami
// Data da criação: 26/12/2020

//Deleta os dados e janelas antigas:
clear;
xdel(winsid());

//Cria a figura do programa principal:
f=figure('figure_position',[325,100],'figure_size',[640,480],'auto_resize','on','background',[8],'figure_name','Euler-Poinsot Solver  |  Developer: Cássio Murakami','dockable','off','infobar_visible','off','toolbar_visible','off','menubar_visible','off','default_axes','on','visible','off');
handles.dummy = 0;
////Frames da janela principal:
handles.frame1 =uicontrol(f,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.0709615,0.6128345,0.31,0.26],'Relief','default','SliderStep',[0.01,0.1],'String','UnName1','Style','frame','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','frame1 ','Callback','')
handles.frame2=uicontrol(f,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.0709615,0.3228345,0.31,0.26],'Relief','default','SliderStep',[0.01,0.1],'String','frame2','Style','frame','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','frame2','Callback','')
handles.frame3=uicontrol(f,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.0709615,0.0428345,0.31,0.26],'Relief','default','SliderStep',[0.01,0.1],'String','UnName3','Style','frame','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','frame3','Callback','')
handles.frame4=uicontrol(f,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.504359,0.2598866,0.4267308,0.614059],'Relief','default','SliderStep',[0.01,0.1],'String','UnName4','Style','frame','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','frame4','Callback','')
handles.frame5=uicontrol(f,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.5519872,0.0555329,0.3353846,0.1198866],'Relief','default','SliderStep',[0.01,0.1],'String','UnName5','Style','frame','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','frame5','Callback','')
////Texts da janela principal:
handles.Title_text=uicontrol(f,'unit','normalized','BackgroundColor',[1,1,1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[30],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','center','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.2560656,0.8889648,0.4858333,0.1052381],'Relief','default','SliderStep',[0.01,0.1],'String','Euler-Poinsot Solver','Style','text','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','Title_text','Callback','')
handles.Simulationsettings_text=uicontrol(f,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[14],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.1365385,0.8049206,0.1946795,0.0530839],'Relief','default','SliderStep',[0.01,0.1],'String','Simulation settings','Style','text','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','Simulationsettings_text','Callback','')
handles.Initialtime_text=uicontrol(f,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.1050641,0.7546939,0.1317949,0.0385488],'Relief','default','SliderStep',[0.01,0.1],'String','Initial Time [s]:','Style','text','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','Initialtime_text','Callback','')
handles.Finaltime_text=uicontrol(f,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.1050641,0.6946939,0.1217949,0.0385488],'Relief','default','SliderStep',[0.01,0.1],'String','Final Time [s]:','Style','text','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','Finaltime_text','Callback','')
handles.Step_text=uicontrol(f,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.1050641,0.6346939,0.1217949,0.0385488],'Relief','default','SliderStep',[0.01,0.1],'String','Step [s]:','Style','text','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','Step_text','Callback','')
handles.Momentsofinertia_text=uicontrol(f,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[14],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.0890385,0.5161905,0.2810897,0.057619],'Relief','default','SliderStep',[0.01,0.1],'String','Principals moments of inertia','Style','text','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','Momentsofinertia_text','Callback','')
handles.Ix_text=uicontrol(f,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.1050641,0.4646939,0.1217949,0.0385488],'Relief','default','SliderStep',[0.01,0.1],'String','Ix [kg m^2]:','Style','text','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','Ix_text','Callback','')
handles.Iy_text=uicontrol(f,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.1050641,0.4046939,0.1217949,0.0385488],'Relief','default','SliderStep',[0.01,0.1],'String','Iy [kg m^2]:','Style','text','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','Iy_text','Callback','')
handles.Iz_text=uicontrol(f,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.1050641,0.3446939,0.1217949,0.0385488],'Relief','default','SliderStep',[0.01,0.1],'String','Iz [kg m^2]:','Style','text','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','Iz_text','Callback','')
handles.Initialconditions_text=uicontrol(f,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[14],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','center','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.1062821,0.2391384,0.2372436,0.0457596],'Relief','default','SliderStep',[0.01,0.1],'String','Initial conditions','Style','text','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','Initialconditions_text','Callback','')
handles.wx0_text=uicontrol(f,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.1050641,0.1846939,0.1217949,0.0385488],'Relief','default','SliderStep',[0.01,0.1],'String','wx0 [rad/s]:','Style','text','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','wx0_text','Callback','')
handles.wy0_text=uicontrol(f,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.1050641,0.1246939,0.1217949,0.0385488],'Relief','default','SliderStep',[0.01,0.1],'String','wy0 [rad/s]:','Style','text','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','wy0_text','Callback','')
handles.wz0_text=uicontrol(f,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.1050641,0.0646939,0.1217949,0.0385488],'Relief','default','SliderStep',[0.01,0.1],'String','wz0 [rad/s]:','Style','text','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','wz0_text','Callback','')
handles.Solution_text=uicontrol(f,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[20],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.6665385,0.7949206,0.1746795,0.0530839],'Relief','default','SliderStep',[0.01,0.1],'String','Solution','Style','text','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','Solution_text','Callback','')
////Edits da janela principal:
handles.initialtime_value=uicontrol(f,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.2508974,0.7512018,0.1097436,0.0494785],'Relief','default','SliderStep',[0.01,0.1],'String','0','Style','edit','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','initialtime_value','Callback','')
handles.finaltime_value=uicontrol(f,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.2508974,0.6912018,0.1097436,0.0494785],'Relief','default','SliderStep',[0.01,0.1],'String','10','Style','edit','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','finaltime_value','Callback','')
handles.Step_value=uicontrol(f,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.2508974,0.6312018,0.1097436,0.0494785],'Relief','default','SliderStep',[0.01,0.1],'String','0.01','Style','edit','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','Step_value','Callback','')
handles.Ix_value=uicontrol(f,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.2508974,0.4612018,0.1097436,0.0494785],'Relief','default','SliderStep',[0.01,0.1],'String','3','Style','edit','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','Ix_value','Callback','')
handles.Iy_value=uicontrol(f,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.2508974,0.4012018,0.1097436,0.0494785],'Relief','default','SliderStep',[0.01,0.1],'String','2','Style','edit','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','Iy_value','Callback','')
handles.Iz_value=uicontrol(f,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.2508974,0.3412018,0.1097436,0.0494785],'Relief','default','SliderStep',[0.01,0.1],'String','1','Style','edit','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','Iz_value','Callback','')
handles.wx0_value=uicontrol(f,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.2508974,0.1812018,0.1097436,0.0494785],'Relief','default','SliderStep',[0.01,0.1],'String','3','Style','edit','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','wx0_value','Callback','')
handles.wy0_value=uicontrol(f,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.2508974,0.1212018,0.1097436,0.0494785],'Relief','default','SliderStep',[0.01,0.1],'String','2','Style','edit','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','wy0_value','Callback','')
handles.wz0_value=uicontrol(f,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.2508974,0.0612018,0.1097436,0.0494785],'Relief','default','SliderStep',[0.01,0.1],'String','1','Style','edit','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','wz0_value','Callback','')
////Buttons da janela principal:
handles.Eulersol_button=uicontrol(f,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','center','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.5669231,0.6933333,0.3072436,0.0857596],'Relief','default','SliderStep',[0.01,0.1],'String','Angular Velocities','Style','pushbutton','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','Eulersol_button','Callback','AngularVelocities_callback(handles)')
handles.Euleranglebutton=uicontrol(f,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','center','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.5669231,0.5933333,0.3072436,0.0857596],'Relief','default','SliderStep',[0.01,0.1],'String','Euler''s Angles','Style','pushbutton','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','Euleranglebutton','Callback','Eulerangle_callback(handles)')
handles.poinsotbutton=uicontrol(f,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','center','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.5669231,0.4933333,0.3072436,0.0857596],'Relief','default','SliderStep',[0.01,0.1],'String','Poinsot''s Construction','Style','pushbutton','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','poinsotbutton','Callback','poinsotbutton_callback(handles)')
handles.Phasebutton=uicontrol(f,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','center','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.5669231,0.3933333,0.3072436,0.0857596],'Relief','default','SliderStep',[0.01,0.1],'String','Phase State','Style','pushbutton','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','Phasebutton','Callback','Phasebutton_callback(handles)')
handles.Momentumenergysurface_button=uicontrol(f,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','center','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.5669231,0.2933333,0.3072436,0.0857596],'Relief','default','SliderStep',[0.01,0.1],'String','Momentum/Energy Surfaces','Style','pushbutton','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','Momentumenergysurface_button','Callback','Momentumenergysurface_button_callback(handles)')
handles.showherpbutton=uicontrol(f,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','center','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.5669231,0.0733333,0.3072436,0.0857596],'Relief','default','SliderStep',[0.01,0.1],'String','Open ''Closed Herpolhode''','Style','pushbutton','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','showherpbutton','Callback','closedbutton_callback(handles)')
////Torna a janela principal visível:
f.visible = "on";

////---------------------- Callback_Angular_Velocities ------------------------////

function AngularVelocities_callback(handles)
//Função responsável por apresentar a solução para as componentes do vetor rotação instantânea.

//Verificação do input do usuário:
check = 1;
//Checagem do instante inicial:
if check == 1 then
    if isnum(handles.initialtime_value.string) == %F then
        check = 0;
        messagebox('Insert a valid number for the initial time.','Error message',"error");
    end
end
//Checagem do instante final:
if check == 1 then
    if isnum(handles.finaltime_value.string) == %F then
        check = 0;
        messagebox('Insert a valid number for the final time.','Error message',"error");
    end
end
//Checagem dos intervalos de iteração:
if check == 1 then
    if isnum(handles.Step_value.string) == %F then
        check = 0;
        messagebox('Insert a valid number for the time step.','Error message',"error");
    end
end
//Checagem do valor para Ix:
if check == 1 then
    if isnum(handles.Ix_value.string) == %F then
        check = 0;
        messagebox('Insert a valid number for Ix.','Error message',"error");
    end
end
//Checagem do valor para Iy:
if check == 1 then
    if isnum(handles.Iy_value.string) == %F then
        check = 0;
        messagebox('Insert a valid number for Iy','Error message',"error");
    end
end
//Checagem do valor para Iz:
if check == 1 then
    if isnum(handles.Iz_value.string) == %F then
        check = 0;
        messagebox('Insert a valid number for Iz','Error message',"error");
    end
end
//Checagem do valor para wx0:
if check == 1 then
    if isnum(handles.wx0_value.string) == %F then
        check = 0;
        messagebox('Insert a valid number for wx0.','Error message',"error");
    end
end
//Checagem do valor para wy0:
if check == 1 then
    if isnum(handles.wy0_value.string) == %F then
        check = 0;
        messagebox('Insert a valid number for wy0.','Error message',"error");
    end
end
//Checagem do valor para wz0:
if check == 1 then
    if isnum(handles.wz0_value.string) == %F then
        check = 0;
        messagebox('Insert a valid number for wz0.','Error message',"error");
    end
end
//Checagem do valor do instante inicial ser menor que o valor do instante final:
if check == 1 then
    if strtod(handles.initialtime_value.string) > strtod(handles.finaltime_value.string) then
        check = 0;
        messagebox('Define the time so that: t_final > t_initial.','Error message',"error");
    end
end
//Checagem de Ix ser um valor válido:
if check == 1 then
    if strtod(handles.Ix_value.string) <= 0 then
        check = 0;
        messagebox('Insert a valid value for Ix.','Error message',"error");
    end
end
//Checagem de Iy ser um valor válido:
if check == 1 then
    if strtod(handles.Iy_value.string) <= 0 then
        check = 0;
        messagebox('Insert a valid value for Iy.','Error message',"error");
    end
end
//Checagem de Iz ser um valor válido:
if check == 1 then
    if strtod(handles.Iz_value.string) <= 0 then
        check = 0;
        messagebox('Insert a valid value for Iz.','Error message',"error");
    end
end
//Checagem da convenção arbitrada Ix ≥ Iy ≥ Iz:
if check == 1 then
    if strtod(handles.Ix_value.string) < strtod(handles.Iy_value.string) | strtod(handles.Ix_value.string) < strtod(handles.Iz_value.string) | strtod(handles.Iy_value.string) < strtod(handles.Iz_value.string) then
        check = 0;
        messagebox('Define the principal moments of inertia so that: Ix ≥ Iy ≥ Iz.','Error message',"error");
    end
end

//Inicialização da solução para as componentes do vetor rotação instantânea:
if check == 1 then
    t0 = strtod(handles.initialtime_value.string);      //Instante inicial [s]
    t_final = strtod(handles.finaltime_value.string);   //Instante final [s]
    step = strtod(handles.Step_value.string);           //Intervalo de iteração [s]
    I1 = strtod(handles.Ix_value.string);               //Momento principal de inércia Ix [kg m^2]
    I2 = strtod(handles.Iy_value.string);               //Momento principal de inércia Iy [kg m^2]
    I3 = strtod(handles.Iz_value.string);               //Momento principal de inércia Iz [kg m^2]
    wx0 = strtod(handles.wx0_value.string);             //Componente inicial X do vetor rotação instantânea [rad/s]
    wy0 = strtod(handles.wy0_value.string);             //Componente inicial Y do vetor rotação instantânea [rad/s]
    wz0 = strtod(handles.wz0_value.string);             //Componente inicial Z do vetor rotação instantânea [rad/s]
    
    //Criação do vetor tempo:
    t = t0:step:t_final
    
    //Criação de vetores nulos:
    wx_nu=zeros(length(t));
    wy_nu=zeros(length(t));
    wz_nu=zeros(length(t));
    
    wx_an=zeros(length(t));
    wy_an=zeros(length(t));
    wz_an=zeros(length(t));
    
    //Definição das constantes principais do problema:
    T = 0.5*(I1*(wx0^2) + I2*(wy0^2) + I3*(wz0^2));     //Energia cinética
    G = sqrt((I1*wx0)^2 + (I2*wy0)^2 + (I3*wz0)^2);     //Módulo do momento da quantidade de movimento
    
    //Definição da integral elíptica de tipo um:
    function y=inicialsn(u,k), y = 1/(sqrt((1-u^2)*(1-(k*u)^2)))
    endfunction
        
    //------------ Solução analítica do problema de Euler-Poinsot -----------//
    
    //Casos particulares de rotação em torno de um eixo principal de inércia:
    if 2*T*I1 == G^2 | 2*T*I2 == G^2 | 2*T*I3 == G^2 then
        for i = 1:length(t)
            wx_an(i) = wx0;
            wy_an(i) = wy0;
            wz_an(i) = wz0;
        end
    
    //Casos particulares de momentos de principais de inércias iguais:
    elseif I1 == I2 then
        for i = 1:length(t) 
            wx_an(i) = sqrt(wx0^2+wy0^2)*sin(((I1-I3)/I1)*wz0*t(i) + atan(wx0,wy0));
            wy_an(i) = sqrt(wx0^2+wy0^2)*cos(((I1-I3)/I1)*wz0*t(i) + atan(wx0,wy0));
            wz_an(i) = wz0;

        end
    elseif I2 == I3 then
        for i = 1:length(t)
            wx_an(i) = wx0;
            wy_an(i) = sqrt(wy0^2+wz0^2)*cos(((I1-I3)/I3)*wx0*t(i) + atan(wz0,wy0));
            wz_an(i) = sqrt(wy0^2+wz0^2)*sin(((I1-I3)/I3)*wx0*t(i) + atan(wz0,wy0));
        end
        
    //Casos gerais em que 2*T*I2 > G^2:
    elseif 2*T*I2 > G^2 then
        //Cálculo das constantes da solução:
        A = sqrt((2*T*I3 - G^2)/(I1*I3 - I1^2));
        B = sqrt((2*T*I3 - G^2)/(I2*I3 - I2^2));
        C = sqrt((2*T*I1 - G^2)/(I1*I3 - I3^2));
        n = sqrt((I2 - I3)*(2*T*I1 - G^2)/(I1*I2*I3));
        k = sqrt((I1-I2)*(G^2 - 2*T*I3)/((I2-I3)*(2*T*I1 - G^2)));
        m = k^2;
        
        //K corresponde a 1/4 do período de oscilação de uma função elíptica sn de parâmetro k:
        K = (1/n)*%k(m); 
    
        //Condições iniciais que garantem que as funções wx,wy,wz iniciem com o mesmo valor numérico de wx0,wy0,wz0:
        if wx0*wz0 > 0 then
            t0 = (1/n)*intg(0,-wy0/B,list(inicialsn,k));
        else
            t0 = 2*K -(1/n)*intg(0,-wy0/B,list(inicialsn,k));
        end
        
        //Determinação de wx analítico:
        cn_st = ellipj("cn",n*(t + t0),m);
        cn = list2vec(cn_st.cn)(:);
        wx_an = (wz0/abs(wz0))*A*cn;
         
        //Determinação de wy analítico:
        sn_st = ellipj("sn",n*(t + t0),m);
        sn = list2vec(sn_st.sn)(:);
        wy_an = -B*sn;
        
        //Determinação de wz analítico:
        dn_st = ellipj("dn",n*(t + t0),m);
        dn = list2vec(dn_st.dn)(:);
        wz_an = (wz0/abs(wz0))*C*dn;
     
    //Casos gerais em que G^2 > 2*T*I2:
    elseif G^2 > 2*T*I2 then
        //Cálculo das constantes da solução:
        A = sqrt((2*T*I3 - G^2)/(I1*I3 - I1^2));
        B = sqrt((2*T*I1 - G^2)/(I1*I2 - I2^2));
        C = sqrt((2*T*I1 - G^2)/(I1*I3 - I3^2));
        n = sqrt((I1 - I2)*(G^2 - 2*T*I3)/(I1*I2*I3));
        k = sqrt((I2 - I3)*(G^2 - 2*T*I1)/((I1-I2)*(2*T*I3 - G^2)));
        m = k^2;
        
        //K corresponde a 1/4 do período de oscilação de uma função elíptica sn de parâmetro k:
        K = (1/n)*%k(m);
        
        //Condições iniciais que garantem que as funções wx,wy,wz iniciem com o mesmo valor numérico de wx0,wy0.wz0 :
        if wx0*wz0 > 0 then 
            t0 = (1/n)*intg(0,-wy0/B,list(inicialsn,k));
        else
            t0 = 2*K -(1/n)*intg(0,-wy0/B,list(inicialsn,k));
        end
        
        //Determinação de wx analítico:
        dn_st = ellipj("dn",n*(t + t0),m);
        dn = list2vec(dn_st.dn)(:);
        wx_an = (wx0/abs(wx0))*A*dn;
         
        //Determinação de wy analítico:
        sn_st = ellipj("sn",n*(t + t0),m);
        sn = list2vec(sn_st.sn)(:);
        wy_an = -B*sn;
        
        //Determinação de wz analítico:
        cn_st = ellipj("cn",n*(t + t0),m);
        cn = list2vec(cn_st.cn)(:);
        wz_an = (wx0/abs(wx0))*C*cn;
    end
    
    //------------ Solução numérica do problema de Euler-Poinsot -----------//
    
    //Definição do vetor de estados:
    funcprot(0);
        function dy= f_euler_numerico(t,y);
            dy(1) = ((I2-I3)/I1)*y(2)*y(3);
            dy(2) = ((I3-I1)/I2)*y(1)*y(3);
            dy(3) = ((I1-I2)/I3)*y(1)*y(2);
        endfunction
    
    //Solução do sistema diferencial a partir de métodos de integração numérica:
    euler_numerico = ode([wx0;wy0;wz0],0,t,f_euler_numerico);
    
    //Armazenamento do resultado em vetores:
    wx_nu = euler_numerico(1,:);
    wy_nu = euler_numerico(2,:);
    wz_nu = euler_numerico(3,:);
    
    //------------------ Cálculo do erro quadrático médio ------------------//
    
    //Definição de valores iniciais para os erros:
    erro_wx = 0;
    erro_wy = 0;
    erro_wz = 0;
    
    //Passo iterativo sobre cada instante de tempo para o cálculo do erro:
    for i = 1:length(t)
        erro_wx = erro_wx + (wx_nu(i) - wx_an(i))^2;
        erro_wy = erro_wy + (wy_nu(i) - wy_an(i))^2;
        erro_wz = erro_wz + (wz_nu(i) - wz_an(i))^2; 
    end
    
    //Raiz quadrada dos resultados para erro obtidos anteriormente: 
    eqm_wx = sqrt(erro_wx/length(t));
    eqm_wy = sqrt(erro_wy/length(t));
    eqm_wz = sqrt(erro_wz/length(t));
    
    //------------------------ Plot dos resultados ------------------------//
    w_plot = scf()
        w_plot.figure_name = 'Euler''s Solution: Angular Velocities';
        w_plot.figure_position = [0,40];
        w_plot.figure_size = [700,650];
        w_plot.infobar_visible = 'off';
        w_plot.toolbar_visible = 'off';
        w_plot.menubar_visible = 'off';
        //Plot ωx:
        subplot(3,1,1);
        title("Angular velocity X component",'fontsize',4);
        xlabel("Time [s]",'fontsize',2);
        ylabel("ωx [rad/s]",'fontsize',3);
        plot(t,wx_an','b');
        plot_wx_nu = plot(t,wx_nu,'b');
        plot_wx_nu.line_style = 5;
        plot_wx_nu.foreground = 18;
        legend(['Analytical';'Numerical']);
        //Plot ωy:
        subplot(3,1,2);
        title("Angular velocity Y component",'fontsize',4);
        xlabel("Time [s]",'fontsize',2);
        ylabel("ωy [rad/s]",'fontsize',3);
        plot(t,wy_an','r');
        plot_wy_nu = plot(t,wy_nu,'r');
        plot_wy_nu.line_style = 5;
        plot_wy_nu.foreground = 28;
        legend(['Analytical';'Numerical']);
        //Plot ωz:
        subplot(3,1,3)
        title("Angular velocity Z component",'fontsize',4);
        xlabel("Time [s]",'fontsize',2);
        ylabel("ωz [rad/s]",'fontsize',3)
        plot_wz = plot(t,wz_an');
        plot_wz.foreground = 13;
        plot_wy_nu = plot(t,wz_nu);
        plot_wy_nu.line_style = 5;
        plot_wy_nu.foreground = 3;
        legend(['Analytical';'Numerical']);
        
        //Adição dos erros quadráticos médios na janela do plot:
        handles.EQM_wx=uicontrol(w_plot,'unit','normalized','BackgroundColor',[1,1,1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.72,0.937,0.1828125,0.0354167],'Relief','default','SliderStep',[0.01,0.1],'String','σ(ωx) = '+string(eqm_wx),'Style','text','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','test','Callback','')
        handles.EQM_wy=uicontrol(w_plot,'unit','normalized','BackgroundColor',[1,1,1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.72,0.605,0.1828125,0.0354167],'Relief','default','SliderStep',[0.01,0.1],'String','σ(ωy) = '+string(eqm_wy),'Style','text','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','test','Callback','')
        handles.EQM_wz=uicontrol(w_plot,'unit','normalized','BackgroundColor',[1,1,1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.72,0.272,0.1828125,0.0354167],'Relief','default','SliderStep',[0.01,0.1],'String','σ(ωz) = '+string(eqm_wz),'Style','text','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','test','Callback','')
end

endfunction

////------------------------ Callback_Euler's_Angles --------------------------////

function Eulerangle_callback(handles)
//Função responsável por apresentar os gráficos da evolução temporal dos Ângulos de Euler.

//Verificação do input do usuário:
check = 1;
//Checagem do instante inicial:
if check == 1 then
    if isnum(handles.initialtime_value.string) == %F then
        check = 0;
        messagebox('Insert a valid number for the initial time.','Error message',"error");
    end
end
//Checagem do instante final:
if check == 1 then
    if isnum(handles.finaltime_value.string) == %F then
        check = 0;
        messagebox('Insert a valid number for the final time.','Error message',"error");
    end
end
//Checagem dos intervalos de iteração:
if check == 1 then
    if isnum(handles.Step_value.string) == %F then
        check = 0;
        messagebox('Insert a valid number for the time step.','Error message',"error");
    end
end
//Checagem do valor para Ix:
if check == 1 then
    if isnum(handles.Ix_value.string) == %F then
        check = 0;
        messagebox('Insert a valid number for Ix.','Error message',"error");
    end
end
//Checagem do valor para Iy:
if check == 1 then
    if isnum(handles.Iy_value.string) == %F then
        check = 0;
        messagebox('Insert a valid number for Iy','Error message',"error");
    end
end
//Checagem do valor para Iz:
if check == 1 then
    if isnum(handles.Iz_value.string) == %F then
        check = 0;
        messagebox('Insert a valid number for Iz','Error message',"error");
    end
end
//Checagem do valor para wx0:
if check == 1 then
    if isnum(handles.wx0_value.string) == %F then
        check = 0;
        messagebox('Insert a valid number for wx0.','Error message',"error");
    end
end
//Checagem do valor para wy0:
if check == 1 then
    if isnum(handles.wy0_value.string) == %F then
        check = 0;
        messagebox('Insert a valid number for wy0.','Error message',"error");
    end
end
//Checagem do valor para wz0:
if check == 1 then
    if isnum(handles.wz0_value.string) == %F then
        check = 0;
        messagebox('Insert a valid number for wz0.','Error message',"error");
    end
end
//Checagem do valor do instante inicial ser menor que o valor do instante final:
if check == 1 then
    if strtod(handles.initialtime_value.string) > strtod(handles.finaltime_value.string) then
        check = 0;
        messagebox('Define the time so that: t_final > t_initial.','Error message',"error");
    end
end
//Checagem de Ix ser um valor válido:
if check == 1 then
    if strtod(handles.Ix_value.string) <= 0 then
        check = 0;
        messagebox('Insert a valid value for Ix.','Error message',"error");
    end
end
//Checagem de Iy ser um valor válido:
if check == 1 then
    if strtod(handles.Iy_value.string) <= 0 then
        check = 0;
        messagebox('Insert a valid value for Iy.','Error message',"error");
    end
end
//Checagem de Iz ser um valor válido:
if check == 1 then
    if strtod(handles.Iz_value.string) <= 0 then
        check = 0;
        messagebox('Insert a valid value for Iz.','Error message',"error");
    end
end
//Checagem da convenção arbitrada Ix ≥ Iy ≥ Iz:
if check == 1 then
    if strtod(handles.Ix_value.string) < strtod(handles.Iy_value.string) | strtod(handles.Ix_value.string) < strtod(handles.Iz_value.string) | strtod(handles.Iy_value.string) < strtod(handles.Iz_value.string) then
        check = 0;
        messagebox('Define the principal moments of inertia so that: Ix ≥ Iy ≥ Iz.','Error message',"error");
    end
end

//Inicialização da solução para os ângulos de Euler:
if check == 1 then
    t0 = strtod(handles.initialtime_value.string);      //Instante inicial [s]
    t_final = strtod(handles.finaltime_value.string);   //Instante final [s]
    step = strtod(handles.Step_value.string);           //Intervalo de iteração [s]
    I1 = strtod(handles.Ix_value.string);               //Momento principal de inércia Ix [kg m^2]
    I2 = strtod(handles.Iy_value.string);               //Momento principal de inércia Iy [kg m^2]
    I3 = strtod(handles.Iz_value.string);               //Momento principal de inércia Iz [kg m^2]
    wx0 = strtod(handles.wx0_value.string);             //Componente inicial X do vetor rotação instantânea [rad/s]
    wy0 = strtod(handles.wy0_value.string);             //Componente inicial Y do vetor rotação instantânea [rad/s]
    wz0 = strtod(handles.wz0_value.string);             //Componente inicial Z do vetor rotação instantânea [rad/s]
    
    //Criação do vetor tempo:
    t = t0:step:t_final
    
    //Criação de vetores nulos:
    wx_nu=zeros(length(t));
    wy_nu=zeros(length(t));
    wz_nu=zeros(length(t));
    
    wx_an=zeros(length(t));
    wy_an=zeros(length(t));
    wz_an=zeros(length(t));
    
    psi_an = zeros(length(t));
    theta_an = zeros(length(t));
    phi_an = zeros(length(t));
    
    psi = zeros(length(t));
    dpsi = zeros(length(t));
    theta = zeros(length(t));
    dtheta = zeros(length(t));
    phi = zeros(length(t));
    dphi = zeros(length(t));
    
    //Definição das constantes principais do problema:
    T = 0.5*(I1*(wx0^2) + I2*(wy0^2) + I3*(wz0^2));     //Energia cinética
    G = sqrt((I1*wx0)^2 + (I2*wy0)^2 + (I3*wz0)^2);     //Módulo do momento da quantidade de movimento
    
    //Definição da integral elíptica de tipo um:
    function y=inicialsn(u,k), y = 1/(sqrt((1-u^2)*(1-(k*u)^2)))
    endfunction
        
    //----------- Solução analítica do problema de Euler-Poinsot -----------//
    
    //Casos particulares de rotação em torno de um eixo principal de inércia:
    if 2*T*I1 == G^2 | 2*T*I2 == G^2 | 2*T*I3 == G^2 then
        for i = 1:length(t)
            wx_an(i) = wx0;
            wy_an(i) = wy0;
            wz_an(i) = wz0;
        end
    
    //Casos particulares de momentos de principais de inércias iguais:
    elseif I1 == I2 then
        for i = 1:length(t) 
            wx_an(i) = sqrt(wx0^2+wy0^2)*sin(((I1-I3)/I1)*wz0*t(i) + atan(wx0,wy0));
            wy_an(i) = sqrt(wx0^2+wy0^2)*cos(((I1-I3)/I1)*wz0*t(i) + atan(wx0,wy0));
            wz_an(i) = wz0;
        end
    elseif I2 == I3 then
        for i = 1:length(t)
            wx_an(i) = wx0;
            wy_an(i) = sqrt(wy0^2+wz0^2)*cos(((I1-I3)/I3)*wx0*t(i) + atan(wz0,wy0));
            wz_an(i) = sqrt(wy0^2+wz0^2)*sin(((I1-I3)/I3)*wx0*t(i) + atan(wz0,wy0));
        end
        
    //Casos gerais em que 2*T*I2 > G^2:
    elseif 2*T*I2 > G^2 then
        //Cálculo das constantes da solução:
        A = sqrt((2*T*I3 - G^2)/(I1*I3 - I1^2));
        B = sqrt((2*T*I3 - G^2)/(I2*I3 - I2^2));
        C = sqrt((2*T*I1 - G^2)/(I1*I3 - I3^2));
        n = sqrt((I2 - I3)*(2*T*I1 - G^2)/(I1*I2*I3));
        k = sqrt((I1-I2)*(G^2 - 2*T*I3)/((I2-I3)*(2*T*I1 - G^2)));
        m = k^2;
        
        //K corresponde a 1/4 do período de oscilação de uma função elíptica sn de parâmetro k:
        K = (1/n)*%k(m); 
    
        //Condições iniciais que garantem que as funções wx,wy,wz iniciem com o mesmo valor numérico de wx0,wy0,wz0:
        if wx0*wz0 > 0 then 
            t0 = (1/n)*intg(0,-wy0/B,list(inicialsn,k));
        else
            t0 = 2*K -(1/n)*intg(0,-wy0/B,list(inicialsn,k));
        end
        
        //Determinação de wx analítico:
        cn_st = ellipj("cn",n*(t + t0),m);
        cn = list2vec(cn_st.cn)(:);
        wx_an = (wz0/abs(wz0))*A*cn;
         
        //Determinação de wy analítico:
        sn_st = ellipj("sn",n*(t + t0),m);
        sn = list2vec(sn_st.sn)(:);
        wy_an = -B*sn;
        
        //Determinação de wz analítico:
        dn_st = ellipj("dn",n*(t + t0),m);
        dn = list2vec(dn_st.dn)(:);
        wz_an = (wz0/abs(wz0))*C*dn;
    
    //Casos gerais em que G^2 > 2*T*I2:
    elseif G^2 > 2*T*I2 then
        //Cálculo das constantes da solução:
        A = sqrt((2*T*I3 - G^2)/(I1*I3 - I1^2));
        B = sqrt((2*T*I1 - G^2)/(I1*I2 - I2^2));
        C = sqrt((2*T*I1 - G^2)/(I1*I3 - I3^2));
        n = sqrt((I1 - I2)*(G^2 - 2*T*I3)/(I1*I2*I3));
        k = sqrt((I2 - I3)*(G^2 - 2*T*I1)/((I1-I2)*(2*T*I3 - G^2)));
        m = k^2;
        
        //K corresponde a 1/4 do período de oscilação de uma função elíptica sn de parâmetro k:
        K = (1/n)*%k(m); 
        
        //Condições iniciais que garantem que as funções wx,wy,wz iniciem com o mesmo valor numérico de wx0,wy0,wz0 :
        if wx0*wz0 > 0 then 
            t0 = (1/n)*intg(0,-wy0/B,list(inicialsn,k));
        else
            t0 = 2*K -(1/n)*intg(0,-wy0/B,list(inicialsn,k));
        end
        
        //Determinação de wx analítico:
        dn_st = ellipj("dn",n*(t + t0),m);
        dn = list2vec(dn_st.dn)(:);
        wx_an = (wx0/abs(wx0))*A*dn;
         
        //Determinação de wy analítico:
        sn_st = ellipj("sn",n*(t + t0),m);
        sn = list2vec(sn_st.sn)(:);
        wy_an = -B*sn;
    
        //Determinação de wz analítico:
        cn_st = ellipj("cn",n*(t + t0),m);
        cn = list2vec(cn_st.cn)(:);
        wz_an = (wx0/abs(wx0))*C*cn;
    end

    //------------- Condições iniciais dos ângulos de Euler -------------//
    
    //Psi inicial:
    psi0 = 0;
    
    //Theta inicial:
    theta0  = acos(I3*wz0/G);
    
    //Phi inicial:
    phi0 = atan(I1*wx0,(I2*wy0));
    
    //Obtenção das variações angulares iniciais:
    A = [sin(theta0)*sin(phi0) , cos(phi0) , 0;
        sin(theta0)*cos(phi0) , -sin(phi0), 0 ;
        cos(theta0) , 0 , 1 ;];
    b = [-wx0 , -wy0, -wz0]';
    sol_linsolve = linsolve(A , b);
    
    dpsi0 = sol_linsolve(1);
    dtheta0 = sol_linsolve(2);
    dphi0 = sol_linsolve(3);
    
    //--------------- Solução analítica dos ângulos de Euler ---------------//
    
    //Caso de indeterminação de phi0:
    if wx0 == 0 & wy0 == 0 then 
        for i = 1:length(t)
            psi(1,i) = 0
            if wz0 > 0
                theta(1,i) = 0;
            else
                theta(1,i) = %pi;
            end
            phi(1,i) = dphi0*(i/length(t))
        end
    
    //Casos gerais:
    else 
    
    //Solução analítica do ângulo de nutação:
    theta_an  = acos(I3*wz_an/G);
        
    //Solução analítica do ângulo de rotação própria:
    phi_an = atan(I1*wx_an,I2*wy_an);
    
    //Solução analítica do ângulo de precessão:
    
        //Casos particulares:
        if I1 == I2 | I2 == I3 | 2*T*I1 == G^2 | 2*T*I2 == G^2 | 2*T*I3 == G^2 then 
            function fun = quad(i)
                fun = G*(I1*wx_an(i)^2 + I2*wy_an(i)^2)/((I1^2)*wx_an(i)^2 + (I2^2)*wy_an(i)^2)
            endfunction
            
            psi_an(1) = psi0;
            for i = 1:length(t) - 1
                psi_an(i+1) = psi_an(i) + (quad(i) + quad(i+1))*(t_final/length(t))/2
            end 
        
        //Caso geral para 2*T*I2 > G^2:
        elseif 2*T*I2 > G^2 then 
            alpha = (I3/I1)*((I1 - I2)/(I2 - I3))
            function fun = ellipj_third(i)
                fun = 1/(1 + alpha*sn(i)^2) 
            endfunction
            
            psi_an_q(1) = psi0;
            for i = 1:length(t) - 1
                psi_an_q(i+1) = psi_an_q(i) + (ellipj_third(i) + ellipj_third(i+1))*(t_final/length(t))/2
            end
            
            for i = 1:length(t)
                psi_an(i) = (G/I3)*t(i) - (G*(I1-I3)/(I1*I3))*psi_an_q(i)
            end 
            
        //Caso geral para G^2 > 2*T*I2:
        else 
            alpha = (I3/I1)*((I1 - I2)/(I2 - I3));
            function fun = ellipj_third(i)
                fun = 1/(1 + alpha*(k^2)*sn(i)^2) 
            endfunction
            
            psi_an_q(1) = psi0;
            for i = 1:length(t) - 1
                psi_an_q(i+1) = psi_an_q(i) + (ellipj_third(i) + ellipj_third(i+1))*(t_final/length(t))/2
            end
            
            for i = 1:length(t)
                psi_an(i) = (G/I3)*t(i) - (G*(I1-I3)/(I1*I3))*psi_an_q(i)
            end
        end
    end
    
    //---------------- Solução numérica dos ângulos de Euler ----------------//
    
    //Definição do vetor de estados:
    funcprot (0);
        function dy = euler_angle(t,y);
            psi = y(1);
            dpsi = y(2);
            theta = y(3);
            dtheta = y(4);
            phi = y(5);
            dphi = y(6);
            
            gammapsi = (I1*sin(phi)^2 + I2*cos(phi)^2)*dpsi*dtheta*sin(2*theta) + (I1 - I2)*(dpsi*dphi*sin(theta)^2*sin(2*phi) + dtheta^2*cos(theta)*sin(phi)*cos(phi) + dtheta*dphi*sin(theta)*cos(2*phi)) - I3*dpsi*dtheta*sin(2*theta) - I3*dphi*dtheta*sin(theta);
            
            gammatheta = (I1 - I2)*(dpsi*dphi*sin(theta)*cos(2*phi) - dtheta*dphi*sin(2*phi)) + I3*dpsi^2*sin(theta)*cos(theta) + I3*dpsi*dphi*sin(theta) - (I1*sin(phi)^2 + I2*cos(phi)^2)*dpsi^2*sin(theta)*cos(theta);
            
            gammaphi = dpsi*dtheta*sin(theta) + ((I1 - I2)/I3)*(dpsi^2*sin(theta)^2*sin(phi)*cos(phi) + dpsi*dtheta*sin(theta)*cos(2*phi) - dtheta^2*sin(phi)*cos(phi));
            
            dy(1) = dpsi;
            dy(2) = ((I1 - I2)/(I1*I2))*(sin(phi)*cos(phi)/sin(theta))*gammatheta - (I3/(I1*I2))*(cos(theta)/sin(theta)^2)*(I1*cos(phi)^2 + I2*sin(phi)^2)*gammaphi - (1/(I1*I2))*(1/sin(theta)^2)*(I1*cos(phi)^2 + I2*sin(phi)^2)*gammapsi;
            dy(3) = dtheta;
            dy(4) = -(I1*I2 + ((I1 - I2)*sin(phi)*cos(phi))^2)*gammatheta/(I1*I2*(I1*cos(phi)^2 + I2*sin(phi)^2)) + (I3*(I1 - I2)/(I1*I2))*cotg(theta)*sin(phi)*cos(phi)*gammaphi + ((I1 - I2)/(I1*I2))*(1/sin(theta))*sin(phi)*cos(phi)*gammapsi;
            dy(5) = dphi;
            dy(6) = - ((I1 - I2)/(I1*I2))*sin(phi)*cos(phi)*cotg(theta)*gammatheta + (1 + (I3/(I1*I2))*cotg(theta)^2*(I1*cos(phi)^2 + I2*sin(phi)^2))*gammaphi + (1/(I1*I2))*(cotg(theta)/sin(theta))*(I1*cos(phi)^2 + I2*sin(phi)^2)*gammapsi;
        endfunction
        
    //Solução do sistema de equações diferenciais por métodos de integração numérica:
    result_euler = ode([psi0;dpsi0;theta0;dtheta0;phi0;dphi0],0,t,euler_angle);
    
    //Armazenamento do resultado em vetores:
    psi = result_euler(1,:);
    dpsi  = result_euler(2,:);
    theta = result_euler(3,:);
    dtheta = result_euler(4,:);
    phi = result_euler(5,:);
    dphi = result_euler(6,:);
    
    //--------- Correção do valor de phi após completar meia volta:----------//
    for i = 1:length(t)
        while phi(i) - phi_an(i) > %pi/2
            phi_an(i) = phi_an(i) + 2*%pi
        end
        while phi_an(i) - phi(i) > %pi/2
            phi_an(i) = phi_an(i) - 2*%pi 
        end
    end
    
    //------------------ Cálculo do erro quadrático médio ------------------//
    
    //Determinação dos valores iniciais para os erros:
    erro_psi = 0;
    erro_theta = 0;
    erro_phi = 0;
    
    //Passo iterativo a cada instante de tempo para o cálculo do erro:
    for i = 1:length(t)
        erro_psi = erro_psi + (psi_an(i) - psi(i))^2;
        erro_theta = erro_theta + (theta_an(i) - theta(i))^2;
        erro_phi = erro_phi + (phi_an(i) - phi(i))^2;
    end
    
    //Raiz quadrada dos resultados obtidos anteriormente:
    eqm_psi = sqrt(erro_psi/length(t));
    eqm_theta = sqrt(erro_theta/length(t));
    eqm_phi = sqrt(erro_phi/length(t));
     
    
    //------------------------ Plot dos resultados ------------------------//
    euler_angles_plot = scf()
        euler_angles_plot.figure_name = 'Euler''s Solution: Euler''s Angles';
        euler_angles_plot.figure_position = [0,40];
        euler_angles_plot.figure_size = [700,650];
        euler_angles_plot.infobar_visible = 'off';
        euler_angles_plot.toolbar_visible = 'off';
        euler_angles_plot.menubar_visible = 'off';
        //Plot ψ:
        subplot(3,1,1);
        title("Precession Angle ψ",'fontsize',4);
        xlabel("Time [s]",'fontsize',2);
        ylabel("ψ [rad]",'fontsize',3);
        plot(t,psi_an','b');
        plot_psi_nu = plot(t,psi,'b');
        plot_psi_nu.line_style = 5;
        plot_psi_nu.foreground = 18;
        legend(['Analytical';'Numerical'],[4]);
        //Plot θ:
        subplot(3,1,2);
        title("Nutation Angle θ",'fontsize',4);
        xlabel("Time [s]",'fontsize',2);
        ylabel("θ [rad]",'fontsize',3);
        plot(t,theta_an','r');
        plot_theta_nu = plot(t,theta,'r');
        plot_theta_nu.line_style = 5;
        plot_theta_nu.foreground = 28;
        legend(['Analytical';'Numerical'],[4]);
        //Plot φ:
        subplot(3,1,3)
        title("Intrinsic Rotation Angle φ",'fontsize',4);
        xlabel("Time [s]",'fontsize',2);
        ylabel("φ [rad]",'fontsize',3)
        plot_phi = plot(t,phi_an');
        plot_phi.foreground = 13;
        plot_phi_nu = plot(t,phi);
        plot_phi_nu.line_style = 5;
        plot_phi_nu.foreground = 3;
        legend(['Analytical';'Numerical'],[4]);
        
        //Adição dos erros quadráticos médios na janela do plot:
        handles.EQM_psi=uicontrol(euler_angles_plot,'unit','normalized','BackgroundColor',[1,1,1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.72,0.937,0.1828125,0.0354167],'Relief','default','SliderStep',[0.01,0.1],'String','σ(ψ) = '+string(eqm_psi),'Style','text','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','test','Callback','')
        handles.EQM_theta=uicontrol(euler_angles_plot,'unit','normalized','BackgroundColor',[1,1,1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.72,0.605,0.1828125,0.0354167],'Relief','default','SliderStep',[0.01,0.1],'String','σ(θ) = '+string(eqm_theta),'Style','text','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','test','Callback','')
        handles.EQM_phi=uicontrol(euler_angles_plot,'unit','normalized','BackgroundColor',[1,1,1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.72,0.272,0.1828125,0.0354167],'Relief','default','SliderStep',[0.01,0.1],'String','σ(φ) = '+string(eqm_phi),'Style','text','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','test','Callback','')
end
endfunction

////------------------------ Callback_Poinsot_Button  --------------------------////

function poinsotbutton_callback(handles)
//Função responsável por apresentar graficamente a solução geométrica de Poinsot.

//Verificação do input do usuário:
check = 1;
//Checagem do instante inicial:
if check == 1 then
    if isnum(handles.initialtime_value.string) == %F then
        check = 0;
        messagebox('Insert a valid number for the initial time.','Error message',"error");
    end
end
//Checagem do instante final:
if check == 1 then
    if isnum(handles.finaltime_value.string) == %F then
        check = 0;
        messagebox('Insert a valid number for the final time.','Error message',"error");
    end
end
//Checagem dos intervalos de iteração:
if check == 1 then
    if isnum(handles.Step_value.string) == %F then
        check = 0;
        messagebox('Insert a valid number for the time step.','Error message',"error");
    end
end
//Checagem do valor para Ix:
if check == 1 then
    if isnum(handles.Ix_value.string) == %F then
        check = 0;
        messagebox('Insert a valid number for Ix.','Error message',"error");
    end
end
//Checagem do valor para Iy:
if check == 1 then
    if isnum(handles.Iy_value.string) == %F then
        check = 0;
        messagebox('Insert a valid number for Iy','Error message',"error");
    end
end
//Checagem do valor para Iz:
if check == 1 then
    if isnum(handles.Iz_value.string) == %F then
        check = 0;
        messagebox('Insert a valid number for Iz','Error message',"error");
    end
end
//Checagem do valor para wx0:
if check == 1 then
    if isnum(handles.wx0_value.string) == %F then
        check = 0;
        messagebox('Insert a valid number for wx0.','Error message',"error");
    end
end
//Checagem do valor para wy0:
if check == 1 then
    if isnum(handles.wy0_value.string) == %F then
        check = 0;
        messagebox('Insert a valid number for wy0.','Error message',"error");
    end
end
//Checagem do valor para wz0:
if check == 1 then
    if isnum(handles.wz0_value.string) == %F then
        check = 0;
        messagebox('Insert a valid number for wz0.','Error message',"error");
    end
end
//Checagem do valor do instante inicial ser menor que o valor do instante final:
if check == 1 then
    if strtod(handles.initialtime_value.string) > strtod(handles.finaltime_value.string) then
        check = 0;
        messagebox('Define the time so that: t_final > t_initial.','Error message',"error");
    end
end
//Checagem de Ix ser um valor válido:
if check == 1 then
    if strtod(handles.Ix_value.string) <= 0 then
        check = 0;
        messagebox('Insert a valid value for Ix.','Error message',"error");
    end
end
//Checagem de Iy ser um valor válido:
if check == 1 then
    if strtod(handles.Iy_value.string) <= 0 then
        check = 0;
        messagebox('Insert a valid value for Iy.','Error message',"error");
    end
end
//Checagem de Iz ser um valor válido:
if check == 1 then
    if strtod(handles.Iz_value.string) <= 0 then
        check = 0;
        messagebox('Insert a valid value for Iz.','Error message',"error");
    end
end
//Checagem da convenção arbitrada Ix ≥ Iy ≥ Iz:
if check == 1 then
    if strtod(handles.Ix_value.string) < strtod(handles.Iy_value.string) | strtod(handles.Ix_value.string) < strtod(handles.Iz_value.string) | strtod(handles.Iy_value.string) < strtod(handles.Iz_value.string) then
        check = 0;
        messagebox('Define the principal moments of inertia so that: Ix ≥ Iy ≥ Iz.','Error message',"error");
    end
end

if check == 1 then
    t0 = strtod(handles.initialtime_value.string);      //Instante inicial [s]
    t_final = strtod(handles.finaltime_value.string);   //Instante final [s]
    step = strtod(handles.Step_value.string);           //Intervalo de iteração [s]
    I1 = strtod(handles.Ix_value.string);               //Momento principal de inércia Ix [kg m^2]
    I2 = strtod(handles.Iy_value.string);               //Momento principal de inércia Iy [kg m^2]
    I3 = strtod(handles.Iz_value.string);               //Momento principal de inércia Iz [kg m^2]
    wx0 = strtod(handles.wx0_value.string);             //Componente inicial X do vetor rotação instantânea [rad/s]
    wy0 = strtod(handles.wy0_value.string);             //Componente inicial Y do vetor rotação instantânea [rad/s]
    wz0 = strtod(handles.wz0_value.string);             //Componente inicial Z do vetor rotação instantânea [rad/s]
    
    //Criação do vetor tempo:
    t = t0:step:t_final
    
    //Criação de vetores nulos:
    wx_nu=zeros(length(t));
    wy_nu=zeros(length(t));
    wz_nu=zeros(length(t));
    
    wx_an=zeros(length(t));
    wy_an=zeros(length(t));
    wz_an=zeros(length(t));
    
    psi_an = zeros(length(t));
    theta_an = zeros(length(t));
    phi_an = zeros(length(t));
    
    psi = zeros(length(t));
    dpsi = zeros(length(t));
    theta = zeros(length(t));
    dtheta = zeros(length(t));
    phi = zeros(length(t));
    dphi = zeros(length(t));
    
    pol_x = zeros(length(t));
    pol_y = zeros(length(t));
    pol_z = zeros(length(t));
    her_x = zeros(length(t));
    her_y = zeros(length(t));
    her_z = zeros(length(t));
    
    //Definição das constantes principais do problema:
    T = 0.5*(I1*(wx0^2) + I2*(wy0^2) + I3*(wz0^2));     //Energia cinética
    G = sqrt((I1*wx0)^2 + (I2*wy0)^2 + (I3*wz0)^2);     //Módulo do momento da quantidade de movimento
    
    //------------ Solução numérica do problema de Euler-Poinsot -----------//
    
    //Definição do vetor de estados:
    funcprot(0);
        function dy= f_euler_numerico(t,y);
            dy(1) = ((I2-I3)/I1)*y(2)*y(3);
            dy(2) = ((I3-I1)/I2)*y(1)*y(3);
            dy(3) = ((I1-I2)/I3)*y(1)*y(2);
        endfunction
    
    //Solução do sistema diferencial por métodos numéricos: 
    euler_numerico = ode([wx0;wy0;wz0],0,t,f_euler_numerico);
    
    //Armazenamento do resultado em vetores:
    wx_nu = euler_numerico(1,:);
    wy_nu = euler_numerico(2,:);
    wz_nu = euler_numerico(3,:);
    
    //---------------------Solução geométrica de Poinsot-----------------------//
    
    //Obtenção das condições de contorno iniciais dos ângulos de Euler:
    
    //Psi inicial:
    psi0 = 0;
    
    //Theta inicial:
    theta0  = acos(I3*wz0/G);
    
    //Phi inicial:
    phi0 = atan(I1*wx0,I2*wy0);
    
    //Obtenção das variações angulares iniciais:
    A = [sin(theta0)*sin(phi0) , cos(phi0) , 0;
        sin(theta0)*cos(phi0) , -sin(phi0), 0 ;
        cos(theta0) , 0 , 1 ;];
    b = [-wx0 , -wy0, -wz0]';
    
    sol_linsolve = linsolve(A , b);
    
    dpsi0 = sol_linsolve(1);
    dtheta0 = sol_linsolve(2);
    dphi0 = sol_linsolve(3);
    
    //---------------- Solução numérica dos ângulos de Euler ----------------//
    
    //Caso de indeterminação de phi0:
    if wx0 == 0 & wy0 == 0 then 
        for i = 1:length(t)
            psi(1,i) = 0
            if wz0 > 0
                theta(1,i) = 0;
            else
                theta(1,i) = %pi;
            end
            phi(1,i) = dphi0*(i/length(t))
        end
    
    //Casos gerais:
    else
    
    //Definição do vetor de estados:
    funcprot (0);
        function dy = euler_angle(t,y);
            psi = y(1);
            dpsi = y(2);
            theta = y(3);
            dtheta = y(4);
            phi = y(5);
            dphi = y(6);
            
            gammapsi = (I1*sin(phi)^2 + I2*cos(phi)^2)*dpsi*dtheta*sin(2*theta) + (I1 - I2)*(dpsi*dphi*sin(theta)^2*sin(2*phi) + dtheta^2*cos(theta)*sin(phi)*cos(phi) + dtheta*dphi*sin(theta)*cos(2*phi)) - I3*dpsi*dtheta*sin(2*theta) - I3*dphi*dtheta*sin(theta);
            
            gammatheta = (I1 - I2)*(dpsi*dphi*sin(theta)*cos(2*phi) - dtheta*dphi*sin(2*phi)) + I3*dpsi^2*sin(theta)*cos(theta) + I3*dpsi*dphi*sin(theta) - (I1*sin(phi)^2 + I2*cos(phi)^2)*dpsi^2*sin(theta)*cos(theta);
            
            gammaphi = dpsi*dtheta*sin(theta) + ((I1 - I2)/I3)*(dpsi^2*sin(theta)^2*sin(phi)*cos(phi) + dpsi*dtheta*sin(theta)*cos(2*phi) - dtheta^2*sin(phi)*cos(phi));
            
            dy(1) = dpsi;
            dy(2) = ((I1 - I2)/(I1*I2))*(sin(phi)*cos(phi)/sin(theta))*gammatheta - (I3/(I1*I2))*(cos(theta)/sin(theta)^2)*(I1*cos(phi)^2 + I2*sin(phi)^2)*gammaphi - (1/(I1*I2))*(1/sin(theta)^2)*(I1*cos(phi)^2 + I2*sin(phi)^2)*gammapsi;
            dy(3) = dtheta;
            dy(4) = -(I1*I2 + ((I1 - I2)*sin(phi)*cos(phi))^2)*gammatheta/(I1*I2*(I1*cos(phi)^2 + I2*sin(phi)^2)) + (I3*(I1 - I2)/(I1*I2))*cotg(theta)*sin(phi)*cos(phi)*gammaphi + ((I1 - I2)/(I1*I2))*(1/sin(theta))*sin(phi)*cos(phi)*gammapsi;
            dy(5) = dphi;
            dy(6) = - ((I1 - I2)/(I1*I2))*sin(phi)*cos(phi)*cotg(theta)*gammatheta + (1 + (I3/(I1*I2))*cotg(theta)^2*(I1*cos(phi)^2 + I2*sin(phi)^2))*gammaphi + (1/(I1*I2))*(cotg(theta)/sin(theta))*(I1*cos(phi)^2 + I2*sin(phi)^2)*gammapsi;
        endfunction
        
        //Solução do sistema diferencial por métodos numéricos:
        result_euler = ode([psi0;dpsi0;theta0;dtheta0;phi0;dphi0],0,t,euler_angle);
        
        //Armazenamento das soluções em vetores:
        psi = result_euler(1,:);
        dpsi  = result_euler(2,:);
        theta = result_euler(3,:);
        dtheta = result_euler(4,:);
        phi = result_euler(5,:);
        dphi = result_euler(6,:);
    end
    
    // ---------------------Plot Poinsot's Construction----------------------//
    Poinsot_plot = scf();
        Poinsot_plot.figure_name = 'Poinsot''s Construction';
        Poinsot_plot.figure_position = [30,80];
        Poinsot_plot.infobar_visible = 'off';
        Poinsot_plot.toolbar_visible = 'off';
        Poinsot_plot.menubar_visible = 'off';
        title("Poinsot''s Construction",'fontsize',5);
        xlabel(" ");
        ylabel(" ");
        zlabel(" ");
        handles.Momentumcolor=uicontrol(Poinsot_plot,'unit','normalized','BackgroundColor',[0,1,0],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.7549359,0.8205669,0.0153205,0.0236054],'Relief','default','SliderStep',[0.01,0.1],'String','UnName1','Style','frame','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','Momentumcolor','Callback','')
    handles.Angularcolor=uicontrol(Poinsot_plot,'unit','normalized','BackgroundColor',[1,0,1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.7549359,0.7650669,0.0153205,0.0236054],'Relief','default','SliderStep',[0.01,0.1],'String','UnName2','Style','frame','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','Angularcolor','Callback','')
    handles.Angular_text=uicontrol(Poinsot_plot,'unit','normalized','BackgroundColor',[1,1,1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.7852564,0.7427438,0.174359,0.0680272],'Relief','default','SliderStep',[0.01,0.1],'String','Angular Velocity','Style','text','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','Angular_text','Callback','')
    handles.Momentum_text=uicontrol(Poinsot_plot,'unit','normalized','BackgroundColor',[1,1,1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.7849359,0.8005669,0.1853205,0.0636054],'Relief','default','SliderStep',[0.01,0.1],'String','Angular Momentum ','Style','text','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','Momentum_text','Callback','')
    handles.Polhodecolor=uicontrol(Poinsot_plot,'unit','normalized','BackgroundColor',[1,0,0],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.7549359,0.7105669,0.0153205,0.0236054],'Relief','default','SliderStep',[0.01,0.1],'String','UnName5','Style','frame','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','Polhodecolor','Callback','')
    handles.Polhode_text=uicontrol(Poinsot_plot,'unit','normalized','BackgroundColor',[1,1,1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.7849359,0.6905669,0.1753205,0.0636054],'Relief','default','SliderStep',[0.01,0.1],'String','Polhode','Style','text','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','Polhode_text','Callback','')
    handles.Herpolhodecolor=uicontrol(Poinsot_plot,'unit','normalized','BackgroundColor',[0,0,1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.7549359,0.6605669,0.0153205,0.0236054],'Relief','default','SliderStep',[0.01,0.1],'String','UnName7','Style','frame','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','Herpolhodecolor','Callback','')
    handles.Herpolhode_text=uicontrol(Poinsot_plot,'unit','normalized','BackgroundColor',[1,1,1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.7849359,0.6405669,0.1753205,0.0636054],'Relief','default','SliderStep',[0.01,0.1],'String','Herpolhode','Style','text','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','Herpolhode_text','Callback','')
    
    //Definição da resolução da elipsóide gerada:
    theta_elipse_resolution = 40;
    phi_elipse_resolution = 20;
    
    //Controle sobre os eixos a serem trabalhados:
    h_axes = gca();
    h_axes.auto_ticks = "off";
    h_axes.hidden_axis_color = -2;
    zoom = 1;
    
    drawlater()
    
    //Construção do elipsóide:
    theta_eli = linspace(0,%pi,theta_elipse_resolution);
    phi_eli = linspace(0,2*%pi,phi_elipse_resolution);
    
    deff("[x,y,z] = eli(theta,phi)",["x=rx*sin(theta).*cos(phi)";"y=ry*sin(theta).*sin(phi)";"z=rz*cos(theta)"]);
    
    rx = sqrt(2*T/I1);
    ry = sqrt(2*T/I2);
    rz = sqrt(2*T/I3);
    
    [x,y,z] = eval3dp(eli,theta_eli,phi_eli);
    
    //Construção dos eixos móveis:
    eixo = gca();
    tamanho_eixo_movel = 1.8;
    tamanho_seta = -1;
    
    xarrows([0 tamanho_eixo_movel*rx],[0 0],[0 0],tamanho_seta);
    eixox_movel = eixo.children;
    eixox_movel.Segs_color = 0;
    
    xarrows([0 0],[0 tamanho_eixo_movel*ry],[0 0],tamanho_seta);
    eixoy_movel = eixo.children(1);
    eixoy_movel.Segs_color = 0;
    
    xarrows([0 0],[0 0],[0 tamanho_eixo_movel*rz],tamanho_seta);
    eixoz_movel = eixo.children(1);
    eixoz_movel.Segs_color = 0;
    
    //Construção do vetor rotação instantânea:
    xarrows([0 wx0],[0 wy0],[0 wz0]);
    vetor_w = eixo.children(1);
    vetor_w.Segs_color = 6;
    vetor_w.thickness = 3;
    
    //Construção do plano invariante:
    x_invplan = [-(1.25*zoom)*rx,(1.25*zoom)*rx]';
    y_invplan = [-(1.25*zoom)*ry,(1.25*zoom)*ry]';
    invplan_height = 2*T/G;
    
    z_invplan = x_invplan*y_invplan'*0 + invplan_height;
    
    inv_plan = plot3d(x_invplan,y_invplan,z_invplan);
    inv_plan.color_mode = 0;
    inv_plan.foreground = -1;
    
    //Construção do vetor momento da quantidade de movimento:
    xarrows([0,0],[0,0],[0,G],-1,3);
    
    //Construção da Herpoloide:
    for k = 1:length (t)
        
        R = [
        cos(psi(k))*cos(phi(k)) - sin(psi(k))*cos(theta(k))*sin(phi(k)) , -cos(psi(k))*sin(phi(k)) - sin(psi(k))*cos(theta(k))*cos(phi(k)), sin(psi(k))*sin(theta(k)) ;
        sin(psi(k))*cos(phi(k)) + cos(psi(k))*cos(theta(k))*sin(phi(k)) , -sin(psi(k))*sin(phi(k)) + cos(psi(k))*cos(theta(k))*cos(phi(k)), -cos(psi(k))*sin(theta(k)) ;
        sin(theta(k))*sin(phi(k)) , sin(theta(k))*cos(phi(k)) , cos(theta(k)) ;
        ];
        
        her = R*[wx_nu(k) wy_nu(k) wz_nu(k)]';
        her_x_full(k) = her(1);
        her_y_full(k) = her(2);
        her_z_full(k) = her(3);
    end
    
    param3d(her_x,her_y,her_z);
    her_conf = eixo.children(1);
    her_conf.foreground = 2; 
    her_conf.thickness = 2;
    
    // Matriz de rotação inicial:
    R_ini = [
        cos(psi(1))*cos(phi(1)) - sin(psi(1))*cos(theta(1))*sin(phi(1)) , -cos(psi(1))*sin(phi(1)) - sin(psi(1))*cos(theta(1))*cos(phi(1)), sin(psi(1))*sin(theta(1)) ;
        sin(psi(1))*cos(phi(1)) + cos(psi(1))*cos(theta(1))*sin(phi(1)) , -sin(psi(1))*sin(phi(1)) + cos(psi(1))*cos(theta(1))*cos(phi(1)), -cos(psi(1))*sin(theta(1)) ;
        sin(theta(1))*sin(phi(1)) , sin(theta(1))*cos(phi(1)) , cos(theta(1)) ;
        ];
        
    for i = 1:length(x')
        T_ini = R_ini*[x'(i) y'(i) z'(i)]';
        x_aux(i) = T_ini(1);
        y_aux(i) = T_ini(2);
        z_aux(i) = T_ini(3);
    end
    
    //Comando que transforma a matriz linha e conserta ela:
    n=1;
    for i = 1:length(x(:,1))
        for j = 1:length(x(1,:))
            x_new(i,j) = x_aux(n);
            y_new(i,j) = y_aux(n);
            z_new(i,j) = z_aux(n);
            n = n + 1;
        end
    end
    
    //Rotação inicial do elipsóide:
    elipse = plot3d(x_new, y_new, z_new); 
    elipse.color_mode = 0;
    elipse.hiddencolor = -1;
    elipse.foreground = -1;
    
    //Construção da Poloide inicial:
    for k = 1:length(t)
        pol_ini = R_ini*[wx_nu(k),wy_nu(k),wz_nu(k)]';
        
        pol_x_full(k) = pol_ini(1);
        pol_y_full(k) = pol_ini(2);
        pol_z_full(k) = pol_ini(3);
    
    end
    
    param3d(pol_x,pol_y,pol_z);
    pol_conf = eixo.children(1);
    pol_conf.foreground = 5;
    pol_conf.thickness = 3;
    
    //Configurações da janela:
    h_axes.rotation_angles = [210,45];
    h_axes.data_bounds = [-zoom*rz,-zoom*rz,-zoom*rz;zoom*rz,zoom*rz,zoom*rz];
    h_axes.box = "off";
    h_axes.axes_visible = "off";
    
    //Looping para a animação:
    for k = 1 : length(t)
        
        R = [
        cos(psi(k))*cos(phi(k)) - sin(psi(k))*cos(theta(k))*sin(phi(k)) , -cos(psi(k))*sin(phi(k)) - sin(psi(k))*cos(theta(k))*cos(phi(k)), sin(psi(k))*sin(theta(k)) ;
        sin(psi(k))*cos(phi(k)) + cos(psi(k))*cos(theta(k))*sin(phi(k)) , -sin(psi(k))*sin(phi(k)) + cos(psi(k))*cos(theta(k))*cos(phi(k)), -cos(psi(k))*sin(theta(k)) ;
        sin(theta(k))*sin(phi(k)) , sin(theta(k))*cos(phi(k)) , cos(theta(k)) ;
        ];
            
        for i = 1:length(x')
            T = R*[x'(i),y'(i),z'(i)]';
            x_aux(i) = T(1);
            y_aux(i) = T(2);
            z_aux(i) = T(3); 
        end
        
        n = 1;
        for i = 1:length(x(:,1))
            for j = 1:length(x(1,:))
                x_new(i,j) = x_aux(n);
                y_new(i,j) = y_aux(n);
                z_new(i,j) = z_aux(n);
                n = n + 1;
             end
         end
    
        //Animação dos eixos móveis:
        eixox_movel.data = [0,0,0; tamanho_eixo_movel*rx*R(1,1) , tamanho_eixo_movel*rx*R(2,1) , tamanho_eixo_movel*rx*R(3,1)];
        eixoy_movel.data = [0,0,0; tamanho_eixo_movel*ry*R(1,2) , tamanho_eixo_movel*ry*R(2,2) , tamanho_eixo_movel*ry*R(3,2)];
        eixoz_movel.data = [0,0,0; tamanho_eixo_movel*rz*R(1,3) , tamanho_eixo_movel*rz*R(2,3) , tamanho_eixo_movel*rz*R(3,3)];
        
        //Animação do vetor rotação instantânea:
        vetor_w.data = [0,0,0; R(1,1)*wx_nu(k) + R(1,2)*wy_nu(k) + R(1,3)*wz_nu(k), R(2,1)*wx_nu(k) + R(2,2)*wy_nu(k) + R(2,3)*wz_nu(k), R(3,1)*wx_nu(k) + R(3,2)*wy_nu(k) + R(3,3)*wz_nu(k)];
        
        //Animação da poloide:
        for l = 1:length(t)
            pol = R*[wx_nu(l),wy_nu(l),wz_nu(l)]';
            pol_x_full(l) = pol(1);
            pol_y_full(l) = pol(2);
            pol_z_full(l) = pol(3);
        end
        
        for l = 1:k
            pol_x(l) = pol_x_full(l);
            pol_y(l) = pol_y_full(l);
            pol_z(l) = pol_z_full(l); 
        end
        
        //Animação da Herpoloide:
        her_x(k) = her_x_full(k);
        her_y(k) = her_y_full(k);
        her_z(k) = her_z_full(k);
        
        //Atualização dos dados:
        drawlater();
        elipse.data.x = x_new;
        elipse.data.y = y_new;
        elipse.data.z = z_new;
        pol_conf.data = [pol_x, pol_y, pol_z];
        her_conf.data = [her_x, her_y, her_z];
        drawnow();
    end
end
endfunction

////------------------------ Callback_Phase_button  --------------------------////

function Phasebutton_callback(handles)
//Função responsável por apresentar o espaço de fases para a situação estudada:

//Verificação do input do usuário:
check = 1;
//Checagem do instante inicial:
if check == 1 then
    if isnum(handles.initialtime_value.string) == %F then
        check = 0;
        messagebox('Insert a valid number for the initial time.','Error message',"error");
    end
end
//Checagem do instante final:
if check == 1 then
    if isnum(handles.finaltime_value.string) == %F then
        check = 0;
        messagebox('Insert a valid number for the final time.','Error message',"error");
    end
end
//Checagem dos intervalos de iteração:
if check == 1 then
    if isnum(handles.Step_value.string) == %F then
        check = 0;
        messagebox('Insert a valid number for the time step.','Error message',"error");
    end
end
//Checagem do valor para Ix:
if check == 1 then
    if isnum(handles.Ix_value.string) == %F then
        check = 0;
        messagebox('Insert a valid number for Ix.','Error message',"error");
    end
end
//Checagem do valor para Iy:
if check == 1 then
    if isnum(handles.Iy_value.string) == %F then
        check = 0;
        messagebox('Insert a valid number for Iy','Error message',"error");
    end
end
//Checagem do valor para Iz:
if check == 1 then
    if isnum(handles.Iz_value.string) == %F then
        check = 0;
        messagebox('Insert a valid number for Iz','Error message',"error");
    end
end
//Checagem do valor para wx0:
if check == 1 then
    if isnum(handles.wx0_value.string) == %F then
        check = 0;
        messagebox('Insert a valid number for wx0.','Error message',"error");
    end
end
//Checagem do valor para wy0:
if check == 1 then
    if isnum(handles.wy0_value.string) == %F then
        check = 0;
        messagebox('Insert a valid number for wy0.','Error message',"error");
    end
end
//Checagem do valor para wz0:
if check == 1 then
    if isnum(handles.wz0_value.string) == %F then
        check = 0;
        messagebox('Insert a valid number for wz0.','Error message',"error");
    end
end
//Checagem do valor do instante inicial ser menor que o valor do instante final:
if check == 1 then
    if strtod(handles.initialtime_value.string) > strtod(handles.finaltime_value.string) then
        check = 0;
        messagebox('Define the time so that: t_final > t_initial.','Error message',"error");
    end
end
//Checagem de Ix ser um valor válido:
if check == 1 then
    if strtod(handles.Ix_value.string) <= 0 then
        check = 0;
        messagebox('Insert a valid value for Ix.','Error message',"error");
    end
end
//Checagem de Iy ser um valor válido:
if check == 1 then
    if strtod(handles.Iy_value.string) <= 0 then
        check = 0;
        messagebox('Insert a valid value for Iy.','Error message',"error");
    end
end
//Checagem de Iz ser um valor válido:
if check == 1 then
    if strtod(handles.Iz_value.string) <= 0 then
        check = 0;
        messagebox('Insert a valid value for Iz.','Error message',"error");
    end
end
//Checagem da convenção arbitrada Ix ≥ Iy ≥ Iz:
if check == 1 then
    if strtod(handles.Ix_value.string) < strtod(handles.Iy_value.string) | strtod(handles.Ix_value.string) < strtod(handles.Iz_value.string) | strtod(handles.Iy_value.string) < strtod(handles.Iz_value.string) then
        check = 0;
        messagebox('Define the principal moments of inertia so that: Ix ≥ Iy ≥ Iz.','Error message',"error");
    end
end

if check == 1 then
    t0 = strtod(handles.initialtime_value.string);      //Instante inicial [s]
    t_final = strtod(handles.finaltime_value.string);   //Instante final [s]
    step = strtod(handles.Step_value.string);           //Intervalo de iteração [s]
    I1 = strtod(handles.Ix_value.string);               //Momento principal de inércia Ix [kg m^2]
    I2 = strtod(handles.Iy_value.string);               //Momento principal de inércia Iy [kg m^2]
    I3 = strtod(handles.Iz_value.string);               //Momento principal de inércia Iz [kg m^2]
    wx0 = strtod(handles.wx0_value.string);             //Componente inicial X do vetor rotação instantânea [rad/s]
    wy0 = strtod(handles.wy0_value.string);             //Componente inicial Y do vetor rotação instantânea [rad/s]
    wz0 = strtod(handles.wz0_value.string);             //Componente inicial Z do vetor rotação instantânea [rad/s]
    
    //Criação do vetor tempo:
    t = t0:step:t_final
    
    //------------ Solução numérica do problema de Euler-Poinsot -----------//
    
    //Definição do vetor de estados:
    funcprot(0);
        function dy= f_euler_numerico(t,y);
            dy(1) = ((I2-I3)/I1)*y(2)*y(3);
            dy(2) = ((I3-I1)/I2)*y(1)*y(3);
            dy(3) = ((I1-I2)/I3)*y(1)*y(2);
        endfunction
    
    //Solução do sistema diferencial a partir de métodos de integração numérica:
    euler_numerico = ode([wx0;wy0;wz0],0,t,f_euler_numerico);
    
    //Armazenamento do resultado em vetores:
    wx_nu = euler_numerico(1,:);
    wy_nu = euler_numerico(2,:);
    wz_nu = euler_numerico(3,:);
    
    // ----------------------- Plot Espaço de fases ------------------------//
    Phase_State_plot = scf();
    Phase_State_plot.figure_name = 'Phase State';
    Phase_State_plot.figure_position = [30,80];
    Phase_State_plot.infobar_visible = 'on';
    Phase_State_plot.toolbar_visible = 'on';
    Phase_State_plot.menubar_visible = 'on';
    title("Phase State",'fontsize',4);
    xlabel("ωx",'fontsize',3);
    ylabel("ωy",'fontsize',3);
    zlabel("ωz",'fontsize',3);
    Phase_State_axes=gca();
    Phase_State_axes.hidden_axis_color = -1;
//    param3d(wx_nu,wy_nu,wz_nu);
    isoview(param3d(wx_nu,wy_nu,wz_nu));
end
endfunction

////------------------------ Callback_Phase_button  --------------------------////

function Momentumenergysurface_button_callback(handles)
//Função responsável por apresentar os elipsóides de energia e momento da quantidade de movimento para a situação estudada:

//Verificação do input do usuário:
check = 1;
//Checagem do instante inicial:
if check == 1 then
    if isnum(handles.initialtime_value.string) == %F then
        check = 0;
        messagebox('Insert a valid number for the initial time.','Error message',"error");
    end
end
//Checagem do instante final:
if check == 1 then
    if isnum(handles.finaltime_value.string) == %F then
        check = 0;
        messagebox('Insert a valid number for the final time.','Error message',"error");
    end
end
//Checagem dos intervalos de iteração:
if check == 1 then
    if isnum(handles.Step_value.string) == %F then
        check = 0;
        messagebox('Insert a valid number for the time step.','Error message',"error");
    end
end
//Checagem do valor para Ix:
if check == 1 then
    if isnum(handles.Ix_value.string) == %F then
        check = 0;
        messagebox('Insert a valid number for Ix.','Error message',"error");
    end
end
//Checagem do valor para Iy:
if check == 1 then
    if isnum(handles.Iy_value.string) == %F then
        check = 0;
        messagebox('Insert a valid number for Iy','Error message',"error");
    end
end
//Checagem do valor para Iz:
if check == 1 then
    if isnum(handles.Iz_value.string) == %F then
        check = 0;
        messagebox('Insert a valid number for Iz','Error message',"error");
    end
end
//Checagem do valor para wx0:
if check == 1 then
    if isnum(handles.wx0_value.string) == %F then
        check = 0;
        messagebox('Insert a valid number for wx0.','Error message',"error");
    end
end
//Checagem do valor para wy0:
if check == 1 then
    if isnum(handles.wy0_value.string) == %F then
        check = 0;
        messagebox('Insert a valid number for wy0.','Error message',"error");
    end
end
//Checagem do valor para wz0:
if check == 1 then
    if isnum(handles.wz0_value.string) == %F then
        check = 0;
        messagebox('Insert a valid number for wz0.','Error message',"error");
    end
end
//Checagem do valor do instante inicial ser menor que o valor do instante final:
if check == 1 then
    if strtod(handles.initialtime_value.string) > strtod(handles.finaltime_value.string) then
        check = 0;
        messagebox('Define the time so that: t_final > t_initial.','Error message',"error");
    end
end
//Checagem de Ix ser um valor válido:
if check == 1 then
    if strtod(handles.Ix_value.string) <= 0 then
        check = 0;
        messagebox('Insert a valid value for Ix.','Error message',"error");
    end
end
//Checagem de Iy ser um valor válido:
if check == 1 then
    if strtod(handles.Iy_value.string) <= 0 then
        check = 0;
        messagebox('Insert a valid value for Iy.','Error message',"error");
    end
end
//Checagem de Iz ser um valor válido:
if check == 1 then
    if strtod(handles.Iz_value.string) <= 0 then
        check = 0;
        messagebox('Insert a valid value for Iz.','Error message',"error");
    end
end
//Checagem da convenção arbitrada Ix ≥ Iy ≥ Iz:
if check == 1 then
    if strtod(handles.Ix_value.string) < strtod(handles.Iy_value.string) | strtod(handles.Ix_value.string) < strtod(handles.Iz_value.string) | strtod(handles.Iy_value.string) < strtod(handles.Iz_value.string) then
        check = 0;
        messagebox('Define the principal moments of inertia so that: Ix ≥ Iy ≥ Iz.','Error message',"error");
    end
end

if check == 1 then
    t0 = strtod(handles.initialtime_value.string);      //Instante inicial [s]
    t_final = strtod(handles.finaltime_value.string);   //Instante final [s]
    step = strtod(handles.Step_value.string);           //Intervalo de iteração [s]
    I1 = strtod(handles.Ix_value.string);               //Momento principal de inércia Ix [kg m^2]
    I2 = strtod(handles.Iy_value.string);               //Momento principal de inércia Iy [kg m^2]
    I3 = strtod(handles.Iz_value.string);               //Momento principal de inércia Iz [kg m^2]
    wx0 = strtod(handles.wx0_value.string);             //Componente inicial X do vetor rotação instantânea [rad/s]
    wy0 = strtod(handles.wy0_value.string);             //Componente inicial Y do vetor rotação instantânea [rad/s]
    wz0 = strtod(handles.wz0_value.string);             //Componente inicial Z do vetor rotação instantânea [rad/s]
    
    //Criação do vetor tempo:
    t = t0:step:t_final
    
    //Definição das constantes da solução:
    T = 0.5*(I1*(wx0^2) + I2*(wy0^2) + I3*(wz0^2));
    G = sqrt((I1*wx0)^2 + (I2*wy0)^2 + (I3*wz0)^2);
    
    //Definição da definição dos elipsóides gerados:
    theta_geo = linspace(0,%pi,100);
    phi_geo = linspace(0,2*%pi,50);
    
    //Função de parametrização das superfícies geradas:
    deff("[x,y,z] = eli(theta,phi)",["x=rx*sin(theta).*cos(phi)";"y=ry*sin(theta).*sin(phi)";"z=rz*cos(theta)"]);
    
    //Construção do elipsóide de energia:
    rx = sqrt(2*T/I1);
    ry = sqrt(2*T/I2);
    rz = sqrt(2*T/I3);
    [wx_geo1,wy_geo1,wz_geo1] = eval3dp(eli,theta_geo,phi_geo);
    
    //Construção do elipsóide do momento da quantidade de movimento:
    rx = G/I1;
    ry = G/I2;
    rz = G/I3;
    [wx_geo2,wy_geo2,wz_geo2] = eval3dp(eli,theta_geo,phi_geo);
    
    //---- Plot Elipsóides de Energia e Momento da quantidade de movimento ----//
    Ellipsoids_plot = scf();
    Ellipsoids_plot.figure_name = 'Momentum/Energy Surfaces';
    Ellipsoids_plot.figure_position = [30,80];
    Ellipsoids_plot.infobar_visible = 'off';
    Ellipsoids_plot.toolbar_visible = 'off';
    Ellipsoids_plot.menubar_visible = 'off';
    title("Momentum/Energy Surfaces",'fontsize',4);
    xlabel("ωx",'fontsize',3);
    ylabel("ωy",'fontsize',3);
    zlabel("ωz",'fontsize',3);
    a=gca();
    a.hidden_axis_color = -1;
    h=get("hdl");
    h.hiddencolor = 2;
    isoview(plot3d(wx_geo1,wy_geo1,wz_geo1, flag = [2,8,4])); //Elipsóide da energia 
    h.hiddencolor = 5; 
    isoview(plot3d(wx_geo2,wy_geo2,wz_geo2, flag = [5,8,4]));//Elipsóide do Momento da quantidade de movimento 
    
    handles.Momentumcolor=uicontrol(Ellipsoids_plot,'unit','normalized','BackgroundColor',[1,0,0],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.7549359,0.7905669,0.0153205,0.0236054],'Relief','default','SliderStep',[0.01,0.1],'String','UnName2','Style','frame','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','Momentumcolor','Callback','')
    handles.Energycolor=uicontrol(Ellipsoids_plot,'unit','normalized','BackgroundColor',[0,0,1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.7549359,0.7105669,0.0153205,0.0236054],'Relief','default','SliderStep',[0.01,0.1],'String','UnName3','Style','frame','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','Energycolor','Callback','')
    handles.Energyellipsoid_text=uicontrol(Ellipsoids_plot,'unit','normalized','BackgroundColor',[1,1,1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.7852564,0.687438,0.174359,0.0680272],'Relief','default','SliderStep',[0.01,0.1],'String','Energy Ellipsoid','Style','text','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','Energyellipsoid_text','Callback','')
    handles.MomentumEllipsoid_text=uicontrol(Ellipsoids_plot,'unit','normalized','BackgroundColor',[1,1,1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.7849359,0.7705669,0.1753205,0.0636054],'Relief','default','SliderStep',[0.01,0.1],'String','Momentum Ellipsoid','Style','text','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','MomentumEllipsoid_text','Callback','')
end
endfunction

////------------------------ Callback_Open_Closed_Herpolhode --------------------------////

function closedbutton_callback(handles)
//Função responsável por abrir a janela do programa de análise da Herpoloide como curva fechada.

//Declaração das variáveis específicas da função como globais:
global Iterations_value;
global error_value;
global Ix_value;
global Iy_value;
global wx0_value;
global wy0_value;
global wz0_value;
global integer_value;
global Iz_value;
global iterationnum_value;

//Cria a figura do programa Closed Herpolhode:
g=figure('figure_position',[575,100],'figure_size',[640,480],'auto_resize','on','background',color("ivory"),'figure_name','Closed Herpolhode  |  Developer: Cássio Murakami','dockable','off','infobar_visible','off','toolbar_visible','off','menubar_visible','off','default_axes','on','visible','off');
handles.dummy = 0;

////Frames: 
handles.frame1 =uicontrol(g,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.0709615,0.6428345,0.31,0.2],'Relief','default','SliderStep',[0.01,0.1],'String','UnName2','Style','frame','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','frame1 ','Callback','')
handles.frame2=uicontrol(g,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.0709615,0.3728345,0.31,0.2],'Relief','default','SliderStep',[0.01,0.1],'String','frame2','Style','frame','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','frame2','Callback','')
handles.frame3=uicontrol(g,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.0709615,0.0628345,0.31,0.26],'Relief','default','SliderStep',[0.01,0.1],'String','UnName4','Style','frame','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','frame3','Callback','')
handles.frame4=uicontrol(g,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.484359,0.4698866,0.4467308,0.404059],'Relief','default','SliderStep',[0.01,0.1],'String','UnName22','Style','frame','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','frame4','Callback','')
////Texts:
handles.Title_text=uicontrol(g,'unit','normalized','BackgroundColor',[1,1,0.94117],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[30],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','center','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.2560656,0.8889648,0.4858333,0.0952381],'Relief','default','SliderStep',[0.01,0.1],'String','Closed Herpolhode','Style','text','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','Title_text','Callback','')
handles.Iteration_settings_Text=uicontrol(g,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[14],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.1465385,0.7849206,0.1946795,0.0530839],'Relief','default','SliderStep',[0.01,0.1],'String','Iteration settings','Style','text','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','Iteration_settings_Text','Callback','')
handles.iteration_Text=uicontrol(g,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.1050641,0.7246939,0.1317949,0.0385488],'Relief','default','SliderStep',[0.01,0.1],'String','Iterations: ','Style','text','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','iteration_value','Callback','')
handles.error_Text=uicontrol(g,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.1050641,0.6646939,0.1217949,0.0385488],'Relief','default','SliderStep',[0.01,0.1],'String','Error Max: ','Style','text','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','error_Text','Callback','')
handles.Momentsofinertia_Text=uicontrol(g,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[14],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.0890385,0.5161905,0.2810897,0.057619],'Relief','default','SliderStep',[0.01,0.1],'String','Principals moments of inertia','Style','text','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','Momentsofinertia_Text','Callback','')
handles.Ix_Text=uicontrol(g,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.1050641,0.4546939,0.1217949,0.0385488],'Relief','default','SliderStep',[0.01,0.1],'String','Ix [kg m^2]:','Style','text','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','Ix_Text','Callback','')
handles.Iy_Text=uicontrol(g,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.1050641,0.3946939,0.1217949,0.0385488],'Relief','default','SliderStep',[0.01,0.1],'String','Iy [kg m^2]:','Style','text','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','Iy_Text','Callback','')
handles.Initialconditions_Text=uicontrol(g,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[14],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','center','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.1062821,0.2691384,0.2372436,0.0457596],'Relief','default','SliderStep',[0.01,0.1],'String','Initial conditions','Style','text','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','Initialconditions_Text','Callback','')
handles.wx0_text=uicontrol(g,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.1050641,0.2046939,0.1217949,0.0385488],'Relief','default','SliderStep',[0.01,0.1],'String','wx0 [rad/s]:','Style','text','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','wx0_text','Callback','')
handles.wy0_text=uicontrol(g,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.1050641,0.1446939,0.1217949,0.0385488],'Relief','default','SliderStep',[0.01,0.1],'String','wy0 [rad/s]:','Style','text','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','wy0_text','Callback','')
handles.wz0_text=uicontrol(g,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.1050641,0.0846939,0.1217949,0.0385488],'Relief','default','SliderStep',[0.01,0.1],'String','wz0 [rad/s]:','Style','text','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','wz0_text','Callback','')
handles.Solution_Text=uicontrol(g,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[20],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.6565385,0.8049206,0.1846795,0.0530839],'Relief','default','SliderStep',[0.01,0.1],'String','Solution','Style','text','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','Solution_Text','Callback','')
handles.Integer_text=uicontrol(g,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.6099359,0.7394263,0.1185256,0.0394785],'Relief','default','SliderStep',[0.01,0.1],'String','Integer λ:','Style','text','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','Integer_text','Callback','')
handles.Iterationnum_text=uicontrol(g,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.5948718,0.5657279,0.161859,0.0521542],'Relief','default','SliderStep',[0.01,0.1],'String','Nº Iteration:','Style','text','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','Iterationnum_text','Callback','')
handles.Iz_text=uicontrol(g,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[14],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.5648718,0.4857279,0.161859,0.0521542],'Relief','default','SliderStep',[0.01,0.1],'String','Iz [kg m^2]:','Style','text','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','Iz_text','Callback','')
//Values:
Iterations_value=uicontrol(g,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.2508974,0.7212018,0.1097436,0.0494785],'Relief','default','SliderStep',[0.01,0.1],'String','1000','Style','edit','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','iterations_text','Callback','')
error_value=uicontrol(g,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.2508974,0.6612018,0.1097436,0.0494785],'Relief','default','SliderStep',[0.01,0.1],'String','0.01','Style','edit','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','error_value','Callback','')
Ix_value=uicontrol(g,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.2508974,0.4512018,0.1097436,0.0494785],'Relief','default','SliderStep',[0.01,0.1],'String','6','Style','edit','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','Ix_value','Callback','')
Iy_value=uicontrol(g,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.2508974,0.3912018,0.1097436,0.0494785],'Relief','default','SliderStep',[0.01,0.1],'String','5','Style','edit','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','Iy_value','Callback','')
wx0_value=uicontrol(g,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.2508974,0.2012018,0.1097436,0.0494785],'Relief','default','SliderStep',[0.01,0.1],'String','3','Style','edit','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','wx0_value','Callback','')
wy0_value=uicontrol(g,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.2508974,0.1412018,0.1097436,0.0494785],'Relief','default','SliderStep',[0.01,0.1],'String','2','Style','edit','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','wy0_value','Callback','')
wz0_value=uicontrol(g,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.2508974,0.0812018,0.1097436,0.0494785],'Relief','default','SliderStep',[0.01,0.1],'String','1','Style','edit','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','wz0_value','Callback','')
integer_value=uicontrol(g,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.7099359,0.7364263,0.1185256,0.0494785],'Relief','default','SliderStep',[0.01,0.1],'String','1','Style','edit','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','integer_value','Callback','')
Iz_value=uicontrol(g,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','off','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.70399103,0.4887982,0.1370513,0.0453515],'Relief','default','SliderStep',[0.01,0.1],'String','','Style','edit','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','Iz_value','Callback','')
iterationnum_value=uicontrol(g,'unit','normalized','BackgroundColor',[1,1,1],'Enable','off','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','left','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.7239103,0.5687982,0.0970513,0.0453515],'Relief','default','SliderStep',[0.01,0.1],'String','','Style','edit','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','iterarionnum_value','Callback','')
//Buttons:
handles.Search_button=uicontrol(g,'unit','normalized','BackgroundColor',[-1,-1,-1],'Enable','on','FontAngle','normal','FontName','Tahoma','FontSize',[12],'FontUnits','points','FontWeight','normal','ForegroundColor',[-1,-1,-1],'HorizontalAlignment','center','ListboxTop',[],'Max',[1],'Min',[0],'Position',[0.578141,0.634263,0.2651923,0.0848299],'Relief','default','SliderStep',[0.01,0.1],'String','Search for Iz','Style','pushbutton','Value',[0],'VerticalAlignment','middle','Visible','on','Tag','Search_button','Callback','Search_callback(handles)')
//Axes: 
handles.plotherp= newaxes();handles.plotherp.margins = [ 0 0 0 0];handles.plotherp.axes_bounds = [0.484359,0.5598866,0.4467308,0.404059];

g.background = color("ivory");
g.visible = "on";

endfunction

////------------------------ Callback_Search_Closed_Herpolhode --------------------------////

function Search_callback(handles)
//Função responsável por procurar iterativamente a condição de Herpoloide como curva fechada.

//Declaração das variáveis específicas da função como globais:
global Iterations_value;
global error_value;
global Ix_value;
global Iy_value;
global wx0_value;
global wy0_value;
global wz0_value;
global integer_value;
global Iz_value;
global iterationnum_value;

//Verificação do input do usuário:
check = 1;
//Verificação se o valor atribuido para o número de iterações é válido:
if check == 1 then
    if isnum(Iterations_value.string) == %F then
        check = 0;
        messagebox('Insert a valid number for the Iterations.','Error message',"error");
    end
end
//Verificação se o valor atribuido para o erro máximo é válido:
if check == 1 then
    if isnum(error_value.string) == %F then
        check = 0;
        messagebox('Insert a valid number for the Erro Max.','Error message',"error");
    end
end
//Verificação se o valor atribuido para Ix é válido:
if check == 1 then
    if isnum(Ix_value.string) == %F then
        check = 0;
        messagebox('Insert a valid number for Ix.','Error message',"error");
    end
end
//Verificação se o valor atribuido para Iy é válido:
if check == 1 then
    if isnum(Iy_value.string) == %F then
        check = 0;
        messagebox('Insert a valid number for Iy','Error message',"error");
    end
end
//Verificação se o valor atribuido para wx0 é válido:
if check == 1 then
    if isnum(wx0_value.string) == %F then
        check = 0;
        messagebox('Insert a valid number for wx0.','Error message',"error");
    end
end
//Verificação se o valor atribuido para wy0 é válido:
if check == 1 then
    if isnum(wy0_value.string) == %F then
        check = 0;
        messagebox('Insert a valid number for wy0.','Error message',"error");
    end
end
//Verificação se o valor atribuido para wz0 é válido:
if check == 1 then
    if isnum(wz0_value.string) == %F then
        check = 0;
        messagebox('Insert a valid number for wz0.','Error message',"error");
    end
end
//Verificação se o valor atribuido para Ix é positivo não nulo:
if check == 1 then
    if strtod(Ix_value.string) <= 0 then
        check = 0;
        messagebox('Insert a valid value for Ix.','Error message',"error");
    end
end
//Verificação se o valor atribuido para Iy é positivo não nulo:
if check == 1 then
    if strtod(Iy_value.string) <= 0 then
        check = 0;
        messagebox('Insert a valid value for Iy.','Error message',"error");
    end
end
//Verificação se o valor atribuido para λ é válido:
if check == 1 then
    if isnum(integer_value.string) == %F then
        check = 0;
        messagebox('Insert a valid value for λ.','Error message',"error");
    end
end
//Verificação se a condição Ix ≥ Iy é satisfeita:
if check == 1 then
    if strtod(Ix_value.string) < strtod(Iy_value.string) then
        check = 0;
        messagebox('Define the principal moments of inertia so that: Ix ≥ Iy.','Error message',"error");
    end
end

//Display de uma janela em branco para a exibição da figura da Herpoloide gerada:
handles.plotherp = newaxes();handles.plotherp.margins = [ 0 0 0 0];handles.plotherp.axes_bounds = [0.484359,0.5598866,0.4467308,0.404059];

if check == 1 then
    step_I3 = strtod(Iterations_value.string);            //Número de iterações para Iz
    erro = strtod(error_value.string);                    //Erro máximo para Iz [kg m^2]
    I1 = strtod(Ix_value.string);                         //Momento principal de inércia correspondente ao eixo X [kg m^2]
    I2 = strtod(Iy_value.string);                         //Momento principal de inércia correspondente ao eixo Y [kg m^2] 
    wx0 = strtod(wx0_value.string);                       //Componente X inicial do vetor rotação instantânea [rad/s] 
    wy0 = strtod(wy0_value.string);                       //Componente Y inicial do vetor rotação instantânea [rad/s]
    wz0 = strtod(wz0_value.string);                       //Componente Z inicial do vetor rotação instantânea [rad/s]
    k_int = strtod(integer_value.string);                 //Inteiro que multiplica 2%pi;
    
    I3 = linspace(0,I2,step_I3);                          //Valores para o momento principal de inércia correspondente ao eixo Z [kg m^2]
    
    //Valores iniciais para o passo iterativo:
    verification = 0;
    i3 = 1;
    
    // Busca do I3 tal que a Herpoloide realize uma curva fechada :
    while verification == 0 & i3 < length(I3)
        
        //Constantes da solução:
        T = 0.5*(I1*(wx0^2) + I2*(wy0^2) + I3(i3)*(wz0^2));
        G = sqrt((I1*wx0)^2 + (I2*wy0)^2 + (I3(i3)*wz0)^2);
    
        //Definição das integral elíptica de tipo um:
        function y=inicialsn(u,k), y = 1/(sqrt((1-u^2)*(1-(k*u)^2)))
        endfunction
            
        //------------ Solução analítica para o ângulo de precessão -----------//
        
        //Casos gerais para 2*T*I2 > G^2:
        if 2*T*I2 > G^2 then
            //Cálculo das constantes da solução :
            A = sqrt((2*T*I3(i3) - G^2)/(I1*I3(i3) - I1^2));
            B = sqrt((2*T*I3(i3) - G^2)/(I2*I3(i3) - I2^2));
            C = sqrt((2*T*I1 - G^2)/(I1*I3(i3) - I3(i3)^2));
            n = sqrt((I2 - I3(i3))*(2*T*I1 - G^2)/(I1*I2*I3(i3)));
            k = sqrt((I1-I2)*(G^2 - 2*T*I3(i3))/((I2-I3(i3))*(2*T*I1 - G^2)));
            m = k^2;
            
            //K corresponde a 1/4 do período de oscilação de uma função elíptica sn de parâmetro k:
            K = (1/n)*%k(m); 
            
            //Condição inicial que garante que as funções wx,wy,wz iniciem com o mesmo valor numérico de wx0,wy0,wz0:
            if wx0*wz0 > 0 then 
                t0 = (1/n)*intg(0,-wy0/B,list(inicialsn,k));
            else
                t0 = 2*K -(1/n)*intg(0,-wy0/B,list(inicialsn,k));
            end
        
            function fun = ellipjpi_case1(x)
                fun = 1/(1 + b*(ellipj(n*(x+t0),m))^2);
            endfunction
            
            a = G*(I1 - I3(i3))/(I1*I3(i3));
            b = I3(i3)*(I1 - I2)/(I1*(I2 - I3(i3))); 
            total_phase = (G/I3(i3))*4*K - a*intg(0,4*K,ellipjpi_case1);
            
        //Casos gerais para G^2 > 2*T*I2:
        elseif G^2 > 2*T*I2 then
            A = sqrt((2*T*I3(i3) - G^2)/(I1*I3(i3) - I1^2));
            B = sqrt((2*T*I1 - G^2)/(I1*I2 - I2^2));
            C = sqrt((2*T*I1 - G^2)/(I1*I3(i3) - I3(i3)^2));
            n = sqrt((I1 - I2)*(G^2 - 2*T*I3(i3))/(I1*I2*I3(i3)));
            k = sqrt((I2 - I3(i3))*(G^2 - 2*T*I1)/((I1-I2)*(2*T*I3(i3) - G^2)));
            m = k^2;
            
            //K corresponde a 1/4 do período de oscilação de uma função elíptica sn de parâmetro k:
            K = (1/n)*%k(m);
            
            if wx0*wz0 > 0 then // Condicional para definir qual ponto dentre os dois possíveis escolher
                t0 = (1/n)*intg(0,-wy0/B,list(inicialsn,k));
            else
                t0 = 2*K -(1/n)*intg(0,-wy0/B,list(inicialsn,k));
            end
            
            function fun = ellipjpi_case2(x)
                fun = 1/(1 + b*(k^2)*(ellipj(n*(x+t0),m))^2);
            endfunction
            
            a = G*(I1 - I3(i3))/(I1*I3(i3));
            b = I3(i3)*(I1 - I2)/(I1*(I2 - I3(i3)));
            total_phase = (G/I3(i3))*4*K - a*intg(0,4*K,ellipjpi_case2);
        end
        
        //Display do número da iteração que está sendo executada:
        iterationnum_value.string = string(i3);
        
        //Verificação se a iteração corresponde ao caso de Herpoloide como curva fechada:
        if erro > abs(total_phase - 2*%pi*k_int)
            verification = 1;
        else
            i3 = i3 + 1;
        end
    end
    
    //Resultados finais:
    if verification == 0 then
        //Caso não seja encontrado Iz no espectro de valores considerados:
        messagebox('Iz was not found for the specified parameters.','Iz was not found',"warning");
    else
        //Display do valor de Iz caso esse seja encontrado:
        Iz_value.string = string(I3(i3));
        
        //--------------- Figura da herpoloide resultante --------------------//
        
        t0 = 0;                                     //Instante inicial [s]
        t_final = 10;                               //Instante final [s]
        step = 0.001;                               //Passo temporal iterativo [s]
        I1 = strtod(Ix_value.string);               //Momento principal de inércia X [kg m^2]
        I2 = strtod(Iy_value.string);               //Momento principal de inércia Y [kg m^2]
        I3 = I3(i3);                                //Momento principal de inércia Z [kg m^2]
        wx0 = strtod(wx0_value.string);             //Componente X inicial do vetor rotação instantânea [rad/s]
        wy0 = strtod(wy0_value.string);             //Componente Y inicial do vetor rotação instantânea [rad/s]
        wz0 = strtod(wz0_value.string);             //Componente Z inicial do vetor rotação instantânea [rad/s]
        
        //Vetor tempo:
        t = t0:step:t_final
        
        //Vetores nulos:
        wx_an=zeros(length(t));
        wy_an=zeros(length(t));
        wz_an=zeros(length(t));
        
        psi = zeros(length(t));
        dpsi = zeros(length(t));
        theta = zeros(length(t));
        dtheta = zeros(length(t));
        phi = zeros(length(t));
        dphi = zeros(length(t));
        
        //Definição das constantes principais do problema:
        T = 0.5*(I1*(wx0^2) + I2*(wy0^2) + I3*(wz0^2));
        G = sqrt((I1*wx0)^2 + (I2*wy0)^2 + (I3*wz0)^2);
        
        //Definição da integral elíptica de tipo um:
        function y=inicialsn(u,k), y = 1/(sqrt((1-u^2)*(1-(k*u)^2)))
        endfunction
            
        //------------ Solução analítica do problema de Euler-Poinsot -----------//
        
        //Casos particulares de rotação em torno de um eixo principal de inércia:
        if 2*T*I1 == G^2 | 2*T*I2 == G^2 | 2*T*I3 == G^2 then
            for i = 1:length(t)
                wx_an(i) = wx0;
                wy_an(i) = wy0;
                wz_an(i) = wz0;
            end
        
        //Casos particulares de momentos de principais de inércias iguais:
        elseif I1 == I2 then
            for i = 1:length(t) 
                wx_an(i) = sqrt(wx0^2+wy0^2)*sin(((I1-I3)/I1)*wz0*t(i) + atan(wx0,wy0));
                wy_an(i) = sqrt(wx0^2+wy0^2)*cos(((I1-I3)/I1)*wz0*t(i) + atan(wx0,wy0));
                wz_an(i) = wz0;
            end
        elseif I2 == I3 then
            for i = 1:length(t)
                wx_an(i) = wx0;
                wy_an(i) = sqrt(wy0^2+wz0^2)*cos(((I1-I3)/I3)*wx0*t(i) + atan(wz0,wy0));
                wz_an(i) = sqrt(wy0^2+wz0^2)*sin(((I1-I3)/I3)*wx0*t(i) + atan(wz0,wy0));
            end
            
        //Casos gerais em que 2*T*I2 > G^2:
        elseif 2*T*I2 > G^2 then
            //Cálculo das constantes da solução:
            A = sqrt((2*T*I3 - G^2)/(I1*I3 - I1^2));
            B = sqrt((2*T*I3 - G^2)/(I2*I3 - I2^2));
            C = sqrt((2*T*I1 - G^2)/(I1*I3 - I3^2));
            n = sqrt((I2 - I3)*(2*T*I1 - G^2)/(I1*I2*I3));
            k = sqrt((I1-I2)*(G^2 - 2*T*I3)/((I2-I3)*(2*T*I1 - G^2)));
            m = k^2;
            
            //K corresponde a 1/4 do período de oscilação de uma função elíptica sn de parâmetro k:
            K = (1/n)*%k(m); 
        
            //Condição inicial que garante que as funções wx,wy,wz iniciem com o mesmo valor numérico de wx0,wy0,wz0:
            if wx0*wz0 > 0 then 
                t0 = (1/n)*intg(0,-wy0/B,list(inicialsn,k));
            else
                t0 = 2*K -(1/n)*intg(0,-wy0/B,list(inicialsn,k));
            end
            
            //Determinação de wx analítico:
            cn_st = ellipj("cn",n*(t + t0),m);
            cn = list2vec(cn_st.cn)(:);
            wx_an = (wz0/abs(wz0))*A*cn;
             
            //Determinação de wy analítico:
            sn_st = ellipj("sn",n*(t + t0),m);
            sn = list2vec(sn_st.sn)(:);
            wy_an = -B*sn;
            
            //Determinação de wz analítico:
            dn_st = ellipj("dn",n*(t + t0),m);
            dn = list2vec(dn_st.dn)(:);
            wz_an = (wz0/abs(wz0))*C*dn;
        
        //Casos gerais em que G^2 > 2*T*I2:
        elseif G^2 > 2*T*I2 then
            //Cálculo das constante da solução
            A = sqrt((2*T*I3 - G^2)/(I1*I3 - I1^2));
            B = sqrt((2*T*I1 - G^2)/(I1*I2 - I2^2));
            C = sqrt((2*T*I1 - G^2)/(I1*I3 - I3^2));
            n = sqrt((I1 - I2)*(G^2 - 2*T*I3)/(I1*I2*I3));
            k = sqrt((I2 - I3)*(G^2 - 2*T*I1)/((I1-I2)*(2*T*I3 - G^2)));
            m = k^2;
            
            //K corresponde a 1/4 do período de oscilação de uma função elíptica sn de parâmetro k:
            K = (1/n)*%k(m);
            
            //Condição inicial que garante que as funções wx,wy,wz iniciem com o mesmo valor numérico de wx0,wy0.wz0 :
            if wx0*wz0 > 0 then
                t0 = (1/n)*intg(0,-wy0/B,list(inicialsn,k));
            else
                t0 = 2*K -(1/n)*intg(0,-wy0/B,list(inicialsn,k));
            end
            
            //Determinação de wx analítico:
            dn_st = ellipj("dn",n*(t + t0),m);
            dn = list2vec(dn_st.dn)(:);
            wx_an = (wx0/abs(wx0))*A*dn;
             
            //Determinação de wy analítico:
            sn_st = ellipj("sn",n*(t + t0),m);
            sn = list2vec(sn_st.sn)(:);
            wy_an = -B*sn;
        
            //Determinação de wz analítico:
            cn_st = ellipj("cn",n*(t + t0),m);
            cn = list2vec(cn_st.cn)(:);
            wz_an = (wx0/abs(wx0))*C*cn;
        end
        
        //------------ Solução numérica dos ângulos de Euler -----------//
        
        //Psi inicial:
        psi0 = 0;
        
        //Theta inicial:
        theta0  = acos(I3*wz0/G);
        
        //Phi inicial:
        phi0 = atan(I1*wx0,I2*wy0);
        
        //Variação angulares iniciais:
        A = [sin(theta0)*sin(phi0) , cos(phi0) , 0;
            sin(theta0)*cos(phi0) , -sin(phi0), 0 ;
            cos(theta0) , 0 , 1 ;];
        b = [-wx0 , -wy0, -wz0]';
        
        sol_linsolve = linsolve(A , b);
        dpsi0 = sol_linsolve(1);
        dtheta0 = sol_linsolve(2);
        dphi0 = sol_linsolve(3);
        
        //Caso de indeterminação de phi0:
        if wx0 == 0 & wy0 == 0 then
            for i = 1:length(t)
                psi(1,i) = 0
                if wz0 > 0
                    theta(1,i) = 0;
                else
                    theta(1,i) = %pi;
                end
                phi(1,i) = dphi0*(i/length(t))
            end
        
        //Casos gerais:
        else 
        
        //Definição do vetor de estados:
        funcprot (0);
            function dy = euler_angle(t,y);
                psi = y(1);
                dpsi = y(2);
                theta = y(3);
                dtheta = y(4);
                phi = y(5);
                dphi = y(6);
                
                gammapsi = (I1*sin(phi)^2 + I2*cos(phi)^2)*dpsi*dtheta*sin(2*theta) + (I1 - I2)*(dpsi*dphi*sin(theta)^2*sin(2*phi) + dtheta^2*cos(theta)*sin(phi)*cos(phi) + dtheta*dphi*sin(theta)*cos(2*phi)) - I3*dpsi*dtheta*sin(2*theta) - I3*dphi*dtheta*sin(theta);
                
                gammatheta = (I1 - I2)*(dpsi*dphi*sin(theta)*cos(2*phi) - dtheta*dphi*sin(2*phi)) + I3*dpsi^2*sin(theta)*cos(theta) + I3*dpsi*dphi*sin(theta) - (I1*sin(phi)^2 + I2*cos(phi)^2)*dpsi^2*sin(theta)*cos(theta);
                
                gammaphi = dpsi*dtheta*sin(theta) + ((I1 - I2)/I3)*(dpsi^2*sin(theta)^2*sin(phi)*cos(phi) + dpsi*dtheta*sin(theta)*cos(2*phi) - dtheta^2*sin(phi)*cos(phi));
                
                dy(1) = dpsi;
                dy(2) = ((I1 - I2)/(I1*I2))*(sin(phi)*cos(phi)/sin(theta))*gammatheta - (I3/(I1*I2))*(cos(theta)/sin(theta)^2)*(I1*cos(phi)^2 + I2*sin(phi)^2)*gammaphi - (1/(I1*I2))*(1/sin(theta)^2)*(I1*cos(phi)^2 + I2*sin(phi)^2)*gammapsi;
                dy(3) = dtheta;
                dy(4) = -(I1*I2 + ((I1 - I2)*sin(phi)*cos(phi))^2)*gammatheta/(I1*I2*(I1*cos(phi)^2 + I2*sin(phi)^2)) + (I3*(I1 - I2)/(I1*I2))*cotg(theta)*sin(phi)*cos(phi)*gammaphi + ((I1 - I2)/(I1*I2))*(1/sin(theta))*sin(phi)*cos(phi)*gammapsi;
                dy(5) = dphi;
                dy(6) = - ((I1 - I2)/(I1*I2))*sin(phi)*cos(phi)*cotg(theta)*gammatheta + (1 + (I3/(I1*I2))*cotg(theta)^2*(I1*cos(phi)^2 + I2*sin(phi)^2))*gammaphi + (1/(I1*I2))*(cotg(theta)/sin(theta))*(I1*cos(phi)^2 + I2*sin(phi)^2)*gammapsi;
            endfunction
            
            //Solução do sistema diferencial por métodos numéricos:
            result_euler = ode([psi0;dpsi0;theta0;dtheta0;phi0;dphi0],0,t,euler_angle);
            
            //Armazenamento dos resultados em vetores:
            psi = result_euler(1,:);
            dpsi  = result_euler(2,:);
            theta = result_euler(3,:);
            dtheta = result_euler(4,:);
            phi = result_euler(5,:);
            dphi = result_euler(6,:);
        end
        
        //Obtenção das componentes do vetor rotação instantânea no espaço inercial:
        for i = 1:length(t)
            WX(i) = dtheta(i)*cos(psi(i)) + dphi(i)*sin(theta(i))*sin(psi(i));
            WY(i) = dtheta(i)*sin(psi(i)) - dphi(i)*sin(theta(i))*cos(psi(i));
            WZ(i) = dphi(i)*cos(theta(i)) + dpsi(i);
        end
        
        //------------- Plot da figura da herpoloide resultante ------------------//
        plot_herp = plot(WX,WY);
        plot_herp = gca()
        plot_herp.auto_ticks = "off";
        plot_herp.hidden_axis_color = -2;
        plot_herp.foreground = [-2 -2 -2];
    end
end
endfunction
